_id: Y1FDdsZ09ESyasXz
_key: '!journal!Y1FDdsZ09ESyasXz'
_stats:
  coreVersion: '12.331'
name: Ghlaunder
pages:
  - _id: ZfxP9YwF4AZcAdsA
    _key: '!journal.pages!Y1FDdsZ09ESyasXz.ZfxP9YwF4AZcAdsA'
    _stats:
      coreVersion: '12.331'
    name: Ghlaunder
    sort: 0
    text:
      content: >-
        <h1>Ghlaunder</h1><h2>The Gossamer King</h2>@Source[PZO9290;pages=41]
        <br /><strong>Pathfinder Wiki</strong> <a
        href="http://www.pathfinderwiki.com/wiki/Ghlaunder">Ghlaunder</a><h3>Details</h3><p><strong>Alignment</strong>
        CE<br /><strong>Pantheon</strong> Other Deities<br /><strong>Areas of
        Concern</strong> Infection, parasites, stagnation<br
        /><strong>Domains</strong> Air, Animal, Chaos, Destruction, Evil<br
        /><strong>Subdomains</strong> Catastrophe, Cloud, Demon (Chaos), Demon
        (Evil), Fur, Insect*, Plague (Evil), Rage, Wind<br /><em>* Requires the
        <u>Acolyte of Apocrypha</u> trait.</em><br /><strong>Favored
        Weapon</strong> Spear<br /><strong>Symbol</strong> Mosquito in
        profile<br /><strong>Sacred Animal(s)</strong> Mosquito<br
        /><strong>Sacred Color(s)</strong> Light gray,
        red</p><h3>Obedience</h3><p>Craft a small poppet in the shape of a flea,
        tick, stirge, or other such plague-carrying creature, using natural
        materials such as straw, and mixing your blood with foul-smelling mud or
        dung to bind the poppet together. As it dries over the course of an
        hour, recite verses invoking virulence, filth, and affliction on the
        living while applying leeches to your flesh. At the end of the hour,
        burn the poppet and the leeches, inhaling the foul vapors while
        meditating on the purging effects of disease on the living while your
        leech-drawn blood burns in sacrifice to Ghlaunder. If you are affected
        by a disease, any ability score damage or drain you would take from that
        disease today is halved; if you would take 1 point of ability score
        damage, you instead take none. You can still contract diseases and
        spread them to others as normal.</p><h3>On
        Golarion</h3><p><strong>Centers of Worship</strong> Mwangi Expanse,
        Sodden Lands, Varisia<br /><strong>Nationality</strong>
        monster</p><h2>Boons - Deific Obedience</h2><h3>
        Evangelist</h3><p>@Source[PZO9290;pages=41]<em></em><br /><strong>1:
        Cult of Contagion (Sp)</strong> <em>ray of sickening<sup>UM</sup> 3/day,
        <em>pox pustules<sup>APG</sup> 2/day, or <em>contagion</em> 1/day<br
        /><strong>2: Nauseating Strike (Ex)</strong> You know how to use others'
        capacity for revulsion as a weapon against them. Three times per day,
        you can make a nauseating strike against an opponent through your
        weapon, causing the target to feel the effects of a sickening disease.
        You must declare your use of this ability before you roll your attack,
        and if your attack misses, the strike is wasted. On a hit, your target
        must succeed at a Fortitude save (DC = 10 + 1/2 your Hit Dice) or be
        nauseated for 1d4 rounds, in addition to the normal weapon damage.<br
        /><strong>3: Debilitating Blight (Sp)</strong> Once per day, you can
        cast <em>greater contagion</em><sup>UM</sup> as a spell-like ability,
        infecting the target with a virulent and debilitating disease that takes
        effect immediately; the DC to resist this disease is 4 higher than
        normal. In addition to the effects of the disease, the subject is
        debilitated with wracking coughs and painful, bursting pustules. The
        incessant sickness imposes a -2 penalty on the target's attack rolls,
        weapon damage rolls, saving throws, skill checks, and ability modifiers
        for a number of rounds equal to your Hit
        Dice.</em></p><h3>Exalted</h3><p>@Source[PZO9290;pages=41]<br
        /><strong>1: Infectious Blighter (Sp)</strong> <em>inflict light
        wounds</em> 3/day, <em>accelerate poison<sup>APG</sup> 2/day, or
        <em>nauseating trail</em><sup>ACG</sup> 1/day<br /><strong>2: Blighting
        Channel (Su)</strong> You bring the desolation of drought and blight to
        the landscapes through which you pass. Three times per day when you
        channel negative energy, you can choose to cause damage to plants and
        plant creatures instead of healing undead. Creatures with the plant type
        within range take 1d6 points of damage plus 1d6 additional points for
        every 2 cleric levels beyond 1st you have (maximum 10d6 at 19th level),
        and can attempt a Fortitude saving throw (using the same DC as for your
        regular uses of channel negative energy) for half damage. All normal
        plants in range immediately wither and die (no saving throw).<br
        /><strong>3: Polluted Servant (Sp)</strong> Ghlaunder gifts his most
        devoted with servants of living filth that fight on the supplicants'
        behalf to sow death and disease. Once per day as a standard action, you
        can summon a hezrou demon (<em>Pathfinder RPG Bestiary</em> 62) to serve
        you. The demon follows your commands for 1 round for every Hit Die you
        have before vanishing back to its home. It doesn't obey commands that
        would make it perform overtly good acts, and if such instructions are
        particularly egregious, they could prompt the demon to attack
        you.</em></p><h3>Sentinel</h3><p>@Source[PZO9290;pages=41]<br
        /><strong>1: Poisonous Penitent (Sp)</strong> <em>nauseating
        dart</em><sup>ACG</sup> 3/day, <em>pernicious poison<sup>UM</sup> 2/day,
        or <em>poison</em> 1/day<br /><strong>2: Bloodletter (Ex)</strong> You
        have trained extensively with Ghlaunder's favored weapon in order to
        cause severe trauma to your enemies, opening their veins so their blood
        might spill forth as an offering to your god. You gain a +1 profane
        bonus on all attack and damage rolls with a spear. In addition, when you
        confirm a critical hit, your target takes 1 point of bleed damage for
        every 2 character levels you have. Bleeding creatures take that amount
        of damage every round at the start of their turns. The bleeding can be
        stopped with a successful DC 15 Heal check or the application of any
        effect that heals hit point damage. Bleed damage from this ability or
        any other effect does not stack with itself. Bleed damage bypasses any
        damage reduction the creature might have.<br /><strong>3: Horrible Blow
        (Sp)</strong> Once per day, you can inflict a debilitating disease upon
        your target through your weapon. You must declare your use of this
        ability before you roll your attack. On a hit, the target is affected as
        if the subject of <em>horrid wilting</em> cast by a wizard of a level
        equal to your Hit Dice (maximum CL 20th), as well as normal weapon
        damage. If you miss with your attack, the use of this ability is
        wasted.</em></p><h2>For Followers of
        Ghlaunder</h2><h3>Feats</h3><p><em>Siphon Channel</em></p><h3>Magic
        Items - Wondrous Items</h3><p><em>Gossamer
        Amberstone</em></p><h3>Traits</h3><p><em>Diseased Heart, Potent
        Concoctions</em></p><h2>Unique Spell
        Rules</h2><em>@Source[PZO9290;pages=44]</em><br
        /><h3>Cleric/Warpriest</h3><p><em>Summon Swarm</em> can be prepared as a
        2nd-level spell</p><h3>Ranger</h3><p><em>Summon Swarm</em> can be
        prepared as a 2nd-level spell</p><h2>Other
        Rules</h2>@Source[PZO9290;pages=44]<br />Clerics, druids, and rangers of
        Ghlaunder, upon first gaining the ability to cast divine spells, may
        choose to affect vermin instead of animals when using animal-oriented
        spells (such as <em>animal shapes, <em>detect animals or plants</em>,
        and <em>hide from animals</em>); these spells can no longer be used to
        affect animals.
      format: 1
    title:
      level: 1
      show: false
    type: text
sort: 0
