_id: tA2WRCHLqrt3K17M
_key: '!journal!tA2WRCHLqrt3K17M'
_stats:
  coreVersion: '12.331'
name: Mephistopheles
pages:
  - _id: HGEOME7GAjQAHwxI
    _key: '!journal.pages!tA2WRCHLqrt3K17M.HGEOME7GAjQAHwxI'
    _stats:
      coreVersion: '12.331'
    name: Mephistopheles
    sort: 0
    text:
      content: >-
        <h1> Mephistopheles</h1><h2>The Crimson
        Son</h2>@Source[PZO90102;pages=70] <br /><strong>Pathfinder
        Wiki</strong> <a
        href="http://www.pathfinderwiki.com/wiki/Mephistopheles">Mephistopheles</a><h3>Details</h3><p><strong>Alignment</strong>
        LE<br /><strong>Pantheon</strong> Archdevils<br /><strong>Areas of
        Concern</strong> Contracts, devils, secrets<br
        /><strong>Domains</strong> Evil, Knowledge, Law, Rune<br
        /><strong>Subdomains</strong> Devil (Evil), Devil (Law), Language,
        Memory, Thought<br /><em>* Requires the <u>Acolyte of Apocrypha</u>
        trait.</em><br /><strong>Favored Weapon</strong> Trident<br
        /><strong>Symbol</strong> Trident and ring<br /><strong>Sacred
        Animal(s)</strong> Mockingbird<br /><strong>Sacred Color(s)</strong>
        Red, yellow</p><h3>Obedience</h3><p>Every day you must lie to someone
        for your own personal gain (this cannot be the same person more than
        once a week). Additionally, once per week, you must write down a new
        secret that you have learned about someone else and burn it as a
        sacrifice to Mephistopheles. Gain a +4 profane bonus to Diplomacy checks
        attempted against targets whose attitudes are unfriendly or
        worse.</p><h2>Boons - Deific Obedience</h2><h3>
        Evangelist</h3><p>@Source[PZO90102;pages=75]<br /><strong>1: Fool Maker
        (Sp)</strong> <em>charm person</em> 3/day, <em>undetectable
        alignment</em> 2/day, or <em>suggestion</em> 1/day<br /><strong>2:
        Persecutor (Ex)</strong> You can use your Bluff skill to demoralize
        opponents and influence an opponent's attitude as though it were the
        Intimidate skill. The effects function the same. Additionally, you may
        use the Knowledge (religion) skill on any check related to knowing about
        or interpreting the law-including infernal contracts.<br /><strong>3:
        Charming Liar (Su)</strong> Whenever you use the Bluff skill, you can
        use the Diplomacy skill to influence an NPC's attitude at the same time.
        Both skill checks are made independently, so even if one fails, the
        other might succeed-suggesting an obvious but charming falsehood or an
        aggravating but convincing lie. Additionally, any spell of the charm
        subschool that you cast has its DC increased by
        2.</em></p><h3>Exalted</h3><p>@Source[PZO90102;pages=75]<br /><strong>1:
        Faith Taker (Sp)</strong> <em>unseen servant</em> 3/day, <em>detect
        thoughts</em> 2/day, or <em>glibness</em> 1/day<br /><strong>2: False
        Priest (Su)</strong> You gain a +4 bonus on Disguise checks to pass as a
        worshiper of another deity and can cast spells using holy symbols other
        than your own. Additionally, for a number of hours per day equal to your
        Charisma modifier (minimum 1), you can disguise your alignment. By
        spending a standard action, you hide your true alignment and instead
        detect as being of the same alignment as the deity whose holy symbol you
        are wearing.<br /><strong>3: Revoke Healing (Su)</strong> A number of
        times per day equal to your Charisma modifier (minimum 1), you can deal
        damage to a creature in your line of sight that you have magically
        healed within the past 24 hours. The damage equals the amount restored
        by the highest-level spell with the healing descriptor that you can
        cast. This may result in you causing more damage than you healed. The
        damage occurs in a single round. In the case of healing spells that heal
        over multiple rounds, only healing provided in the first round is
        counted when determining
        damage.</em></p><h3>Sentinel</h3><p>@Source[PZO90102;pages=75]<br
        /><strong>1: Trust Breaker (Sp)</strong> <em>doom</em> 3/day,
        <em>eagle's splendor</em> 2/day, or <em>keen edge</em> 1/day<br
        /><strong>2: Spectacular Rival (Su)</strong> A number of times per day
        equal to your Charisma modifier (minimum 1), you can add your Charisma
        modifier (minimum 1) to your combat maneuver bonus.<br /><strong>3:
        Fatal Choice (Su)</strong> Once per day, upon defeating an opponent, you
        can heal it and attempt to force it to obey you. As soon as you reduce a
        living creature to -1 or fewer hit points, you can force it to attempt a
        Will saving throw with a DC equal to 10 + your Hit Dice + your Charisma
        modifier. If it succeeds, it begins dying as normal. If the target
        fails, it is restored to 1 hit point and is affected as per the spell
        <em>charm monster</em>. The target can willingly fail this saving throw.
        This effect lasts for a number of minutes equal to your Charisma
        modifier (minimum 1). At the end of this period, the target can behave
        as normal.</em></p><h2>Boons - Fiendish
        Obedience</h2><h3>Evangelist</h3><p>@Source[PZO1139;pages=74]<br
        /><strong>1: Whispers from Caina (Sp)</strong> <em>detect secret
        doors</em> 3/day, <em>augury</em> 2/day, or <em>blood biography</em>
        1/day<br /><strong>2: Enter the Hellfire Testament (Su)</strong> As a
        standard action, you can forge deep and unsettling ties to the places
        and individuals around you. While this ability is in effect, an enemy
        that comes within 5 feet of you must succeed at a Will saving throw (DC
        = 10 + half your Hit Dice + your Charisma modifier) or be compelled to
        blurt out a single secret that looms heavy in its mind. This could be a
        personally embarrassing detail, or it could be secret plans to attack or
        sabotage you, at the GM's discretion. Additionally, creatures that fail
        their Will saving throws become confused for as long as you maintain
        this supernatural ability (once you've activated it, maintaining the
        Hellfire Testament is a free action). You can dismiss the Hellfire
        Testament as a free action, and you can use this ability for a number of
        rounds per day equal to your Hit Dice. The duration need not be used all
        at once, but it must be used in 1-round increments. Regardless of its
        saving throw result, a creature subjected to this ability once can't be
        affected by it again for 24 hours. This is a mind-affecting compulsion
        effect.<br /><strong>3: Confide in the Crimson Son (Su)</strong> You
        have an uncanny, infernal knack for earning the trust of those with weak
        minds and weaker wills. Three times per day as a standard action, you
        can target a living creature and offer dark whispers of camaraderie and
        flattery. Unless the creature succeeds at a Will saving throw (DC = 10 +
        half your Hit Dice + your Charisma modifier), it believes you are
        incredibly trustworthy and will refuse to harm you with any of its
        attacks or abilities for a number of rounds equal to your Hit Dice, and
        you gain a +20 profane bonus on Diplomacy checks to influence its
        attitude or make a request. If any of its allies attempt to harm you
        during this period, the creature will defend you however it is able. A
        creature can't be affected by this ability more than once in a 24-hour
        period. This is a mind-affecting
        effect.</em></p><h3>Exalted</h3><p>@Source[PZO1139;pages=74]<br
        /><strong>1: Fulfilled by Hell (Sp)</strong> <em>memorize page </em>
        3/day, <em>honeyed tongue</em> 2/day, or <em>marionette possession</em>
        1/day<br /><strong>2: Signed with Visineir (Su)</strong> The full weight
        of the Crimson Son backs the infernal contracts you sign, even if the
        signatories are coerced or otherwise tricked into agreement. Three times
        per day as a standard action, you can target a creature to bind it into
        a special Hell-backed contract. If the creature fails a Will saving
        throw (DC = 10 + half your Hit Dice + your Charisma modifier), for a
        number of rounds equal to your Hit Dice it cannot make any bodily
        contact with you, including attacking with manufactured weapons, as if
        it were a good-aligned summoned creature and you were affected with a
        <em>protection from good</em> spell. You can attack the affected
        creature without penalty. Every time the creature attempts to attack you
        while this ability is in effect, it is nauseated for 1 round (no saving
        throw).<br /><strong>3: Eternal Contractual Damnation (Sp)</strong> You
        can marshal the might of Hell to briefly imprison your enemies. This
        functions as <em>mass icy prison</em>, using your Hit Dice as your
        caster level, except the targets are trapped in infernal flames and the
        prison deals fire damage instead of cold damage. Affected creatures can
        quench the shifting wall of flames in the same way they could break an
        icy prison. (<em>Create water has no effect on the flames, but casting a
        <em>quench</em> spell automatically extinguishes a single creature's
        prison).</em></p><h3>Sentinel</h3><p>@Source[PZO1139;pages=74]<br
        /><strong>1: Call of the Crimson Son (Sp)</strong> <em>keen senses </em>
        3/day, <em>blood armor</em> 2/day, or <em>chain of perdition</em>
        1/day<br /><strong>2: Bound to the Eighth (Sp)</strong> You are well
        versed in the complex and draconian laws of Hell and those that bind
        devils to service, and you can imitate this binding magic to weaken your
        foes. Once per day as a standard action, you can cast <em>symbol of
        weakness</em>.<br /><strong>3: King of Devils (Sp)</strong> You can call
        upon the infernal power of the legions of devils that answer to
        Mephistopheles to strengthen your body and mind. Once per day, you can
        summon two infernally loyal barbed devils to your side as if using
        <em>summon monster IX</em>.</em></p><h2><em>For Followers of
        Mephistopheles</em></h2><h3>Feats</h3><p><em>Hellish
        Shackles</em></p><h3>Spells</h3><p><em>Seer's
        Bane</em></p><h3>Traits</h3><p><em>Flames of Hell</em></p><h2>Unique
        Spell Rules</h2><em>@Source[PZO90102;pages=72]</em><br
        /><h3>Cleric/Warpriest</h3></em><p><em>Message can be prepared as a
        0-level spell<br /><em>Undetectable Alignment</em> can be prepared as a
        1st-level spell<br /><em>False Vision</em> can be prepared as a
        5th-level spell<br /></em></p><h2>Unique Summon
        Rules</h2>@Source[PZO90102;pages=72]<br /><strong>Summon Monster
        IV</strong>: Guardian Scroll<br /><strong>Summon Monster VIII</strong>:
        Contract Devil (Phistophilus)<br /><strong>Summon Monster IX</strong>:
        Heresy Devil (Ayngavhaul)
      format: 1
    title:
      level: 1
      show: false
    type: text
sort: 0
