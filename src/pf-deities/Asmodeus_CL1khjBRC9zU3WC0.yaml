_id: CL1khjBRC9zU3WC0
_key: '!journal!CL1khjBRC9zU3WC0'
_stats:
  coreVersion: '12.331'
name: Asmodeus
pages:
  - _id: WAN7XG5TYhdMEtCt
    _key: '!journal.pages!CL1khjBRC9zU3WC0.WAN7XG5TYhdMEtCt'
    _stats:
      coreVersion: '12.331'
    name: Asmodeus
    sort: 0
    text:
      content: >-
        <h1>Asmodeus</h1><h2>Prince of Darkness</h2>@Source[PZO9267;pages=20]
        <br /><strong>Pathfinder Wiki</strong> <a
        href="http://www.pathfinderwiki.com/wiki/Asmodeus">Asmodeus</a><h3>Details</h3><p><strong>Alignment</strong>
        LE<br /><strong>Pantheon</strong> Core Deities<br /><strong>Other
        Pantheons</strong> Archdevils, Kobold Deities, Order of the God Claw
        Pantheon<br /><strong>Areas of Concern</strong> Contracts, pride,
        slavery, tyranny<br /><strong>Domains</strong> Evil, Fire, Law, Magic,
        Trickery<br /><strong>Subdomains</strong> Arcane, Ash, Corruption,
        Deception, Devil (Evil), Devil (Law), Divine, Greed, Legislation (Law),
        Rites*, Smoke, Soverignty<br /><em>* Requires the <u>Acolyte of
        Apocrypha</u> trait.</em><br /><strong>Favored Weapon</strong> Mace<br
        /><strong>Symbol</strong> Red pentagram<br /><strong>Sacred
        Animal(s)</strong> Serpent<br /><strong>Sacred Color(s)</strong> Black,
        red</p><h3>Obedience</h3><p>Using a ruby-bladed knife, inscribe
        symmetrical cuts into the flesh of another creature-preferably an
        unwilling sentient being you own or hold dominion over. The blade may be
        solid ruby or forged of metal and edged with serrated ruby fragments.
        Devout priests of Asmodeus take pride in crafting elaborate daggers made
        entirely of ruby. Drain the victim's blood into a bowl of bone made from
        the skull of a sentient humanoid. The amount of blood drained is up to
        you; you don't have to drain so much that you make the creature weak or
        too useless to serve you. Use the bowl of blood to draw a large
        pentagram on the ground. Kneel within the pentagram and concentrate on
        the glory you will bring to the Prince of Darkness's name. Gain a +4
        profane bonus on saving throws against fire effects.</p><h3>Divine
        Gift</h3><p>@Source[PZO1141;pages=74] <br /><em>Nethys Note: See here
        for details on how to gain a Divine Gift</em><br />Asmodeus grants the
        service of a devil to aid in a specific task (which can last no longer
        than 9 nights). The type of devil is generally one whose CR is equal to
        the character's level (or in the case of an NPC, the character's
        CR).</p><h3>On Golarion</h3><p><strong>Centers of Worship</strong>
        Cheliax, Isger, Nidal<br /><strong>Nationality</strong>
        devil</p><h2>Boons - Deific Obedience</h2><h3>
        Evangelist</h3><p>@Source[PZO9267;pages=20]<br /><strong>1: Pitiless
        Judgment (Sp)</strong> <em>wrath<sup>APG</sup> 3/day, <em>flames of the
        faithful<sup>APG</sup> 2/day, or <em>bestow curse</em> 1/day<br
        /><strong>2: Tireless Judgment (Ex)</strong> You gain Favored
        Judgment<sup>UM</sup> as a bonus feat, choosing chaotic outsider, good
        outsider, or a subtype of humanoid. If you don't have the judgment class
        feature, you instead gain a +4 profane bonus on Survival checks made to
        track a creature or individual. This boon doesn't grant you any ranks in
        the Survival skill; therefore, if you have no ranks, you still can
        follow tracks only if the DC of the task is 10 or lower.<br /><strong>3:
        Resounding Judgment (Sp)</strong> Once per day, you can channel the
        effects of <em>resounding blow</em><sup>APG</sup> through your weapon,
        though you don't need to cast (or even know) the spell. You must declare
        your use of this ability before you make the attack roll. On a hit, the
        target is affected as if you had cast <em>resounding blow </em>before
        your attack, and the surrounding area rings with the sound of vicious,
        booming laughter. You don't gain the stunning effect of the spell unless
        you have access to the judgment or smite ability. If your attack misses,
        the <em>resounding blow </em>effect is
        wasted.</em></p><h3>Exalted</h3><p>@Source[PZO9267;pages=20]<br
        /><strong>1: Darkfire (Sp)</strong> <em>burning hands</em> 3/day,
        <em>darkness</em> 2/day, or <em>deeper darkness</em> 1/day<br
        /><strong>2: Embersight (Su)</strong> Your eyes take on the appearance
        of red-hot, glowing embers, granting you the ability to see in darkness
        much like devils. You gain darkvision to a range of 60 feet. If you
        chose either <em>darkness or <em>deeper darkness </em>as the spell-like
        ability granted by your first boon, you can also see perfectly through
        both <em>darkness and <em>deeper darkness</em>. If you already have
        darkvision to a range of 60 feet or more, instead increase the range of
        your darkvision by 10 feet. Your eyes make you extremely distinctive,
        causing you to take a -4 penalty on Disguise checks.<br /><strong>3:
        Hellfire Blast (Sp)</strong> You can use <em>delayed blast fireball
        </em>once per day as a spell-like ability to throw a sphere of
        soulscouring hellfire. The hellfire is a distinctive mixture of black
        and crimson flames in which screaming devilish faces can be seen
        twisting and writhing. Half the damage from this spell is fire, while
        the other half is unholy. This damage modification applies only to the
        <em>delayed blast fireball </em>you create through this boon, not to any
        other spells, effects, or
        attacks.</em></p><h3>Sentinel</h3><p>@Source[PZO9267;pages=20]<br
        /><strong>1: Unholy Warrior (Sp)</strong> <em>protection from good </em>
        3/day, <em>death knell</em> 2/day, or <em>defile armor<sup>APG</sup>
        1/day<br /><strong>2: Deceitful Duelist (Ex)</strong> Your devotion to
        the Prince of Darkness has imbued you with some of his trickery. Three
        times per day, you can attempt a feint as a swift action. You gain a +4
        profane bonus on your Bluff check when attempting to feint using this
        ability. If you successfully attack a creature that has lost its
        Dexterity bonus to AC as a result of your feint, you deal an additional
        1d6 points of damage. This is in addition to any other precision-based
        damage you deal-such as from a sneak attack-and isn't multiplied on a
        critical hit.<br /><strong>3: Diabolical Resistances (Su)</strong> Your
        dark patron rewards your faith with a few drops of devilish blood in
        your veins, granting you a measure of the resilience enjoyed by
        devilkind. Your skin takes on a ruddy cast, and your teeth grow slightly
        sharper. To a casual observer you may look no different, but anyone who
        studies you closely notices these traits. You gain fire resistance 10
        and a +4 profane bonus on saving throws against
        poison.</em></p><h2>Boons - Fiendish
        Obedience</h2><h3>Evangelist</h3><p>@Source[PZO1139;pages=26]<br
        /><strong>1: Pitiless Judgment (Sp)</strong> <em>wrath </em> 3/day,
        <em>flames of the faithful</em> 2/day, or <em>bestow curse</em> 1/day<br
        /><strong>2: Tireless Judgment (Ex)</strong> You gain Favored Judgment
        as a bonus feat, choosing chaotic outsider, good outsider, or a subtype
        of humanoid. If you don't have the judgment class feature, you instead
        gain a +4 profane bonus on Survival checks to track a creature or
        individual of this type. This boon doesn't grant you any ranks in the
        Survival skill; therefore, if you have no ranks, you still can follow
        tracks only if the DC of the task is 10 or lower.<br /><strong>3:
        Resounding Judgment (Sp)</strong> Once per day, you can channel the
        effects of <em>resounding blow</em> through your weapon, though you
        don't need to cast (or even know) the spell. You must declare your use
        of this ability before you roll for the attack. On a hit, the target is
        affected as if you had cast <em>resounding blow</em> before your attack,
        and the surrounding area rings with the sound of vicious, booming
        laughter. You don't gain the stunning effect of the spell unless you
        have access to the judgment or smite ability. If your attack misses, the
        resounding blow effect is
        wasted.</em></p><h3>Exalted</h3><p>@Source[PZO1139;pages=26]<br
        /><strong>1: Darkfire (Sp)</strong> <em>burning hands </em> 3/day,
        <em>darkness</em> 2/day, or <em>deeper darkness</em> 1/day<br
        /><strong>2: Embersight (Su)</strong> Your eyes take on the appearance
        of red-hot, glowing embers, granting you the ability to see in darkness
        much like devils. You gain darkvision to a range of 60 feet. If you
        chose either darkness or deeper darkness as the spell-like ability
        granted by your first boon, you can also see perfectly through both
        darkness and deeper darkness. If you already have darkvision to a range
        of 60 feet or more, the range of your darkvision instead increases by 10
        feet. Your eyes make you extremely distinctive, causing you to take a -4
        penalty on Disguise checks.<br /><strong>3: Hellfire Blast (Sp)</strong>
        The fires of Hell itself are yours to command. You can cast <em>delayed
        blast fireball</em> once per day as a spell-like ability, hurling a
        sphere of soul-scouring hellfire. The hellfire is a mixture of black and
        crimson flames in which screaming, devilish faces can be seen twisting
        and writhing. Half the damage from this spell is fire damage, while the
        other half is unholy damage. This damage modification applies only to
        the <em>delayed blast fireball</em> you create through this boon, not to
        any other attacks, effects, or
        spells.</em></p><h3>Sentinel</h3><p>@Source[PZO1139;pages=26]<br
        /><strong>1: Unholy Warrior (Sp)</strong> <em>protection from good </em>
        3/day, <em>death knell</em> 2/day, or <em>defile armor</em> 1/day<br
        /><strong>2: Deceitful Duelist (Ex)</strong> Your devotion to the Prince
        of Darkness has imbued you with some of his trickery. Three times per
        day, you can attempt a feint as a swift action, with a +4 profane bonus
        on your Bluff check. If you hit a creature that has lost its Dexterity
        bonus to AC as a result of your feint, you deal an additional 1d6 points
        of damage. This stacks with other precision-based damage you deal, such
        as from a sneak attack, and isn't multiplied on a critical hit.<br
        /><strong>3: Diabolical Resistances (Su)</strong> Your dark patron
        rewards your faith with a few drops of devilish blood in your veins,
        granting you a measure of the resilience enjoyed by devilkind. Your skin
        takes on a ruddy cast, and your teeth grow slightly sharper. To a casual
        observer you may look no different, but anyone who studies you closely
        notices these traits. You gain fire resistance 10 and a +4 profane bonus
        on saving throws against poison.</em></p><h2>Divine Fighting
        Technique</h2><h3>Asmodeus's
        Mandate</h3><p><em>@Source[PZO9472;pages=28] </em><br />Although
        soldiers and mercenaries alike in Cheliax claim that the martial
        techniques detailed within <em>Wrath and Punishment</em>, the most
        well-known fighting manual of the Church of Asmodeus, are as old as time
        itself, martial experts across the Inner Sea region note that <em>Wrath
        and Punishment</em> is less than 3 decades old. Of course, few in
        Cheliax dare to publicly make such claims. Outside of Cheliax, copies of
        <em>Wrath and Punishment</em> tend to be found in Hellknight
        enclaves.</em></p><p><em><strong>Initial Benefit</strong>: Whenever you
        threaten a critical hit with a light mace or heavy mace on a foe, that
        foe becomes sickened for 1 round. If you confirm the critical hit, the
        sickened effect lasts for 2 rounds. The duration of the sickened effect
        doesn't stack with multiple critical
        threats.</em></p><p><em><strong>Advanced Prerequisites</strong>: Int 13;
        Combat Expertise; Divine Fighting Technique; Improved Feint; base attack
        bonus +10 or Bluff 10 ranks.</em></p><p><em><strong>Advanced
        Benefit</strong>: An opponent that you hit with a light mace or heavy
        mace who is denied his Dexterity bonus to AC is also hindered by the
        attack. You can attempt a dirty trick<sup>APG</sup> combat maneuver
        check as a swift action that doesn't provoke an attack of opportunity
        immediately after damage from the attack is resolved.</em></p><h2>For
        Followers of Asmodeus</h2><h3>Archetypes</h3><p><em>Demagogue (Bard),
        Hamatulatsu Master (Monk), Hungry Ghost Monk (Monk), Infiltrator
        (Inquisitor)</em></p><h3>Feats</h3><p><em>Conversion Channel, Devilish
        Pride, Diabolical Negotiator, Hamatulatsu</em></p><h3>Magic Items -
        Altars</h3><p><em>Altar of Asmodeus</em></p><h3>Magic Items -
        Armor</h3><p><em>Half-Plate of the Dark Prince</em></p><h3>Magic Items -
        Rings</h3><p><em>Profane Seal Signet</em></p><h3>Magic Items -
        Weapons</h3><p><em>Hell's Eye</em></p><h3>Magic Items - Wondrous
        Items</h3><p><em>Barbed Pentacle of Asmodeus, Circlet of Persuasion,
        Diabolical Masquerade Mask, Infernal Cord (Greater), Hamatulatsu Robe,
        Infernal Cord (Normal), Pentacle
        Token</em></p><h3>Monsters</h3><p><em>Accomplice Devil (Hesperian),
        Basileus (Herald)</em></p><h3>Spells</h3><p><em>Beguiling Gift, Burning
        Gaze, Infernal Healing, Infernal Healing, Greater, Planar Binding,
        Shared Sacrifice, Spellcasting Contract, Spellcasting Contract, Greater,
        Spellcasting Contract, Lesser, Vision of
        Hell</em></p><h3>Traits</h3><p><em>Asmodean Demon Hunter, Contract
        Master, Demon Hunter, Fiendish Confidence, Liar's
        Tongue</em></p><h2>Unique Spell
        Rules</h2><em>@Source[PZO9267;pages=27]<em></em><br
        /><h3>Cleric/Warpriest</h3><p><em>Geas, Lesser</em> can be prepared as a
        4th-level spell<br /><em>Geas/Quest</em> can be prepared as a 5th-level
        spell [variant, allows a Will saving throw]</p><h2>Unique Summon
        Rules</h2>@Source[PZO9029;pages=67]<br /><strong>Summon Monster
        II</strong>: Hell Hound - LE<br /><strong>Summon Monster IV</strong>:
        Cerberi - LE<br /><strong>Summon Monster V</strong>: Bearded Devil - LE
      format: 1
    title:
      level: 1
      show: false
    type: text
sort: 0
