_id: OM3eV76AghVEiRFM
_key: '!journal!OM3eV76AghVEiRFM'
_stats:
  coreVersion: '12.331'
name: Milani
pages:
  - _id: BW8gvHvk1wDkBqzq
    _key: '!journal.pages!OM3eV76AghVEiRFM.BW8gvHvk1wDkBqzq'
    _stats:
      coreVersion: '12.331'
    name: Milani
    sort: 0
    text:
      content: >-
        <h1>Milani</h1><h2>The Everbloom</h2>@Source[PZO9290;pages=71] <br
        /><strong>Pathfinder Wiki</strong> <a
        href="http://www.pathfinderwiki.com/wiki/Milani">Milani</a><h3>Details</h3><p><strong>Alignment</strong>
        CG<br /><strong>Pantheon</strong> Other Deities<br /><strong>Areas of
        Concern</strong> Devotion, hope, uprisings<br /><strong>Domains</strong>
        Chaos, Good, Healing, Liberation, Protection<br
        /><strong>Subdomains</strong> Azata (Chaos), Azata (Good), Defense,
        Freedom, Purity, Restoration, Revolution, Riot<br /><em>* Requires the
        <u>Acolyte of Apocrypha</u> trait.</em><br /><strong>Favored
        Weapon</strong> Morningstar<br /><strong>Symbol</strong> Rose on bloody
        street<br /><strong>Sacred Animal(s)</strong> Mouse<br /><strong>Sacred
        Color(s)</strong> Red, white</p><h3>Obedience</h3><p>Spend time
        meditating among roses you have planted yourself, so you can inhale
        their sacred scent while offering prayers to Milani. If no such roses
        are available, you can instead brew tea from herbs and rose petals and
        share the tea with close friends or neighbors. Preferred topics for
        conversation during this teatime include hopes for the future and
        preparations for times of need, but the act of sharing is itself enough.
        During times of war or conflict, though, you must instead spend time
        sparring, preferably with friends or neighbors whom you plan to fight
        alongside during the conflicts to come. If you are imprisoned and unable
        to spar, you can instead sing any song of hope or resistance in unison
        with one or more fellow prisoners. You gain a +2 sacred bonus on all
        saving throws against charm and compulsion effects, and a +2 sacred
        bonus on all rolls made to dispel or remove such effects from
        others.</p><h3>On Golarion</h3><p><strong>Centers of Worship</strong>
        Cheliax, Galt, Irrisen, Isger, Rahadoum<br
        /><strong>Nationality</strong> half-elf</p><h2>Boons - Deific
        Obedience</h2><h3> Evangelist</h3><p>@Source[PZO9290;pages=71]<br
        /><strong>1: Voice of the Everbloom (Sp)</strong> <em>command</em>
        3/day, <em>enthrall</em> 2/day, or <em>suggestion</em> 1/day<br
        /><strong>2: Inspiring Presence (Su)</strong> Your mere presence
        bolsters your companions to fight harder in the face of even the most
        overwhelming odds and bolsters their hearts against growing weary of
        battle. Once per day as a standard action, you can provide all allies
        within 30 feet of you with a +1 sacred bonus on attack rolls, saving
        throws, and weapon damage rolls for a number of rounds equal to your Hit
        Dice.<br /><strong>3: Invoke Uprising (Sp)</strong> Even shackles of the
        mind do not escape your notice, and you can use an inspiring word from
        the teachings of Milani to help others find the strength to break those
        shackles. You are automatically aware of any creature within 10 feet of
        you that is currently under the effects of a charm, compulsion, or
        possession effect. Three times per day as a swift action, you can
        inspire such a creature to throw off the influence, granting that
        creature a new saving throw to immediately end the effect. If the effect
        does not normally allow a saving throw, calculate the save DC as normal
        if it is a spell; if it's not a spell, the DC is equal to 10 + 1/2 the
        source's Hit Dice + the source's Charisma modifier. The creature gains a
        sacred bonus on this saving throw equal to your Charisma bonus (minimum
        +1). This bonus is doubled if you include a physical touch as part of
        your invocation to rise up against the effect. In either case, this is a
        language-dependent
        effect.</em></p><h3>Exalted</h3><p>@Source[PZO9290;pages=71]<br
        /><strong>1: Sacred Partisan (Sp)</strong> <em>divine favor</em> 3/day,
        <em>spiritual weapon</em> 2/day, or <em>magic vestment</em> 1/day<br
        /><strong>2: Alleyport (Sp)</strong> As the patron deity of urban
        uprisings, the Everbloom grants you the power to appear wherever in a
        settlement, dungeon, or other tight space you are most needed, or to
        escape to fight another day. Once per day as a swift action, you can
        teleport as per <em>dimension door</em>, but only when you are in an
        area no wider than your space, and you can arrive in only an area of
        similar width.<br /><strong>3: Wall of Roses (Sp)</strong> You can call
        upon Milani's symbolic roses to defend the innocent and the righteous
        while stymieing oppressors and their minions. Once per day, you can cast
        <em>wall of thorns</em>. The wall consists of a dense tangle of roses
        through which you and other worshipers of Milani can pass with ease. The
        wall of roses heals from damage dealt to it at a rate of 5 hit points
        per round; it is immune to fire damage; and all piercing damage it deals
        bypasses damage reduction as if it were a good, magic, and silver
        weapon. Evil and lawful creatures damaged by a wall of roses
        automatically become sickened for the next minute (this is a poison
        effect).</em></p><h3>Sentinel</h3><p>@Source[PZO9290;pages=71]<br
        /><strong>1: Neighborhood Guardian (Sp)</strong> <em>protection from
        evil</em> 3/day, <em>shield other</em> 2/day, or <em>magic circle
        against evil</em> 1/day<br /><strong>2: Stoic Guardian (Ex)</strong>
        Inspired by Milani's valor and steadfastness, you refuse to let magic
        corrupt your thoughts and deny fear any hold on your actions. You are
        immune to fear and charm effects, and gain a +4 sacred bonus on all
        saving throws against compulsion effects.<br /><strong>3: Martyrdom
        (Su)</strong> Your deeds and faith have earned you the Everbloom's
        blessing to give your life for a worthy cause and still fight on, and
        she shelters you in battle. As an immediate action once per day,
        whenever a single creature within 300 feet of you is slain by an effect
        or hit point damage, you can redirect that effect or damage onto
        yourself. You gain no saving throw to reduce effects redirected in this
        manner. If the effect kills you, you are restored to life in 1d4 rounds,
        as per <em>resurrection</em>, but once this resurrection effect occurs,
        you lose the ability to use martyrdom for 1 year.</em></p><h2>For
        Followers of Milani</h2><h3>Archetypes</h3><p><em>Champion of the Faith
        (Warpriest), Crusader (Cleric), Divine Strategist (Cleric), Hidden
        Priest (Cleric), Kintargo Rebel (Rogue), Urban Ranger (Ranger),
        Wilderness Medic (Ranger)</em></p><h3>Feats</h3><p><em>Beacon of
        Hope</em></p><h3>Magic Items - Armor</h3><p><em>Milanite
        Armor</em></p><h3>Magic Items - Weapons</h3><p><em>Enduring Bloom,
        Everbloom Thorn, Everbloom's Rose</em></p><h3>Prestige
        Classes</h3><p><em>Rose Warden</em></p><h3>Spells</h3><p><em>Martyr's
        Bargain, Martyr's Last Blessing, Peasant
        Armaments</em></p><h3>Traits</h3><p><em>Split-Second Defense, Talented
        Organizer</em></p><h2>Unique Spell
        Rules</h2><em>@Source[PZO9290;pages=74]</em><br
        /><h3>Cleric/Warpriest</h3><p><em>Coordinated Effort</em> can be
        prepared as a 3rd-level spell<br /><em>Good Hope</em> can be prepared as
        a 3rd-level spell</p><h3>Inquisitor</h3><p><em>Good Hope can be prepared
        as a 3rd-level spell<br /></em></p><h3>Ranger</h3><p><em>Coordinated
        Effort</em> can be prepared as a 3rd-level spell<br /><em>Good Hope can
        be prepared as a 3rd-level spell<br /><em>Remove Fear</em> can be
        prepared as a 1st-level spell<br /><em>Imbue with Spell Ability</em> can
        be prepared as a 3rd-level spell<br /></em></p><h2>Unique Summon
        Rules</h2>@Source[PZO9068;pages=69]<br /><strong>Summon Monster
        I</strong>: Great Horned Owl (extraplanar)<br /><strong>Summon Monster
        IV</strong>: Hound Archon - CG<br /><h2>Other
        Rules</h2>@Source[PZO9290;pages=72]<br />Milani's priests understand
        that a revolutionary's work is often rewarded with death, and they
        accept that they may one day be called to die for a cause. Fortunately,
        the goddess teaches that the truly devout who are martyred saving other
        people from death or tyranny will rise again in some way-perhaps even
        immediately, though being reborn in the faith is much more likely.<p>
        Some priests claim to be the incarnations of past followers of the
        goddess, able to access memories of past lives with the proper magic and
        meditation. A follower born after Milani ascended to godhood (about a
        century ago) can attempt to search for his past lives' memories by
        casting <em>legend lore</em>, which Milani grants only for this purpose
        to clerics, inquisitors, and warpriests as a 4th-level divine spell. The
        first time a caster attempts this use of the spell, he rolls 1d4-1 to
        determine the number of specific followers of Milani whose memories he
        accesses; each time he uses the spell, it reveals only information known
        to those individuals. If the result of the roll is 0, the caster can
        access no memories, either because he is not a reincarnated follower of
        Milani or because the goddess has not made his past lives' memories
        available.</p>
      format: 1
    title:
      level: 1
      show: false
    type: text
sort: 0
