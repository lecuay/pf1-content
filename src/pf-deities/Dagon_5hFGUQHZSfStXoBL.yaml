_id: 5hFGUQHZSfStXoBL
_key: '!journal!5hFGUQHZSfStXoBL'
_stats:
  coreVersion: '12.331'
name: Dagon
pages:
  - _id: CIUi24O89xxAaSTX
    _key: '!journal.pages!5hFGUQHZSfStXoBL.CIUi24O89xxAaSTX'
    _stats:
      coreVersion: '12.331'
    name: Dagon
    sort: 0
    text:
      content: >-
        <h1>Dagon</h1><h2>The Shadow in the Sea</h2>@Source[PZO9267;pages=320]
        <br /><strong>Pathfinder Wiki</strong> <a
        href="http://www.pathfinderwiki.com/wiki/Dagon">Dagon</a><h3>Details</h3><p><strong>Alignment</strong>
        CE<br /><strong>Pantheon</strong> Demon Lords<br /><strong>Areas of
        Concern</strong> Deformity, sea monsters, the sea<br
        /><strong>Domains</strong> Chaos, Destruction, Evil, Water<br
        /><strong>Subdomains</strong> Catastrophe, Demon (Chaos), Demon (Evil),
        Oceans, Rage<br /><em>* Requires the <u>Acolyte of Apocrypha</u>
        trait.</em><br /><strong>Favored Weapon</strong> Trident<br
        /><strong>Symbol</strong> Eye surrounded by runes<br /><strong>Sacred
        Animal(s)</strong> Fish<br /><strong>Sacred Color(s)</strong> Blue,
        gold</p><h3>Obedience</h3><p>Offer a bowl of fresh blood to Dagon by
        speaking prayers over the blood and then emptying the blood into the
        sea. The bowl must be made of gold and inscribed with runes sacred to
        Dagon-such a bowl must be worth no less than 100 gp, but can be reused
        for multiple obediences. Gain a +4 profane bonus against the
        extraordinary or supernatural attacks of creatures with the aquatic or
        water subtype.</p><h2>Boons - Demonic
        Obedience</h2><h3>Demoniac</h3><p>@Source[PZO9225;pages=14]<br
        /><strong>1: First Oath (Sp)</strong> <em>speak with animals (aquatic
        animals only) 3/ day, <em>disfiguring touch </em>(see page 48) 2/day, or
        <em>summon monster III </em>(aquatic creatures only) 1/day<br
        /><strong>2: Second Oath (Ex)</strong> You become immune to damage from
        water pressure, and gain the ability to breathe water, a +2 profane
        bonus to Constitution, and a swim speed equal to your base land speed
        (or increase your current swim speed by 30 ft.).<br /><strong>3: Third
        Oath (Sp)</strong> <em>Dominate monster</em> 1/day (aquatic creatures
        only).</em></p><h2>Boons - Fiendish
        Obedience</h2><h3>Evangelist</h3><p>@Source[PZO1139;pages=40]<br
        /><strong>1: Enslave The Sea (Sp)</strong> <em>hydraulic push</em>
        3/day, <em>slipstream</em> 2/day, or <em>water breathing</em> 1/day<br
        /><strong>2: Aspect of Ishiar (Su)</strong> You can cause the flesh and
        bone of your body to liquefy into a roiling mass of brackish sea water
        as a move action. While under this effect, your body takes on a slick,
        watery appearance and can stretch and shift with ease. You become mostly
        transparent, as if you were composed of liquid, granting you a +4 bonus
        on Stealth checks. While underwater, this bonus increases to +8, and you
        can attempt Stealth checks while observed and without needing cover or
        concealment. You gain DR 10/slashing and your reach increases by 10
        feet. In addition, you can pass through small holes or narrow openings,
        even mere cracks, with anything you were carrying when you activated
        this ability (except other creatures). Finally, you gain the water
        subtype and a swim speed of 60 feet, and you can breathe both water and
        air for the duration of this effect. You can use this ability for 10
        minutes per Hit Die per day; this duration need not be consecutive but
        must be spent in 10-minute increments.<br /><strong>3: Drowning Doom
        (Sp)</strong> You can use <em>mass suffocation</em> once per day, but
        instead of drawing the air from creatures' lungs, this effect fills
        creatures' lungs with anoxic water (and as such, the ability to breathe
        water offers no protection). While disturbing, this water is largely a
        cosmetic effect and doesn't otherwise adjust how the spell effect
        functions.</em></p><h3>Exalted</h3><p>@Source[PZO1139;pages=40]<br
        /><strong>1: First Oath (Sp)</strong> <em>speak with animals (aquatic
        animals only) 3/day, <em>disfiguring touch</em> 2/day, or <em>summon
        monster III</em> (aquatic creatures only) 1/day<br /><strong>2: Second
        Oath (Ex)</strong> You become immune to damage from water pressure and
        gain the ability to breathe water, a +2 profane bonus to Constitution,
        and a swim speed equal to your base speed (if you already have a swim
        speed, it increases by 30 feet instead).<br /><strong>3: Third Oath
        (Sp)</strong> You can cast <em>dominate monster</em> as a spell-like
        ability once per day but can target only aquatic creatures or creatures
        currently breathing
        water.</em></p><h3>Sentinel</h3><p>@Source[PZO1139;pages=40]<br
        /><strong>1: Teratogen (Sp)</strong> <em>long arm</em> 3/day, <em>bear's
        endurance</em> 2/day, or <em>resinous skin</em> 1/day<br /><strong>2:
        Mutagenic Strike (Su)</strong> A number of times per day equal to your
        Hit Dice, when you strike a creature with a melee weapon, as a swift
        action you can cause it to become deformed. When you do so, the creature
        struck must succeed at a Fortitude save (DC = 10 + half your Hit Dice +
        your Charisma modifier) or take 2 points of Charisma drain. A creature
        reduced to 0 Charisma in this way is permanently transformed into a
        hideously deformed aquatic version of itself; it gains the mutant
        template, the aquatic subtype, and the amphibious quality, and its
        alignment becomes chaotic evil. When this transformation occurs, all
        Charisma damage the creature has suffered is instantly cured. Such
        mutants are initially friendly toward you. This transformation is a
        curse effect.<br /><strong>3: Horror of the Deep (Su)</strong> As a move
        action, you can switch between your natural form and a hideous hybrid of
        your natural form and a deep-sea monstrosity. In your hybrid form, you
        are immune to damage from water pressure, gain the ability to breathe
        water, and gain a swim speed equal to twice your base speed. Your head
        becomes that of a viperfish, granting you a bite attack that deals
        damage as for a creature two size categories larger than you (2d6 for a
        Medium creature). In addition, a single long tentacle sprouts from your
        body, granting you a tentacle secondary natural attack that deals damage
        as for a creature of your size category (1d4 for a Medium creature).
        This tentacle has the grab and constrict abilities; the constrict damage
        is equal to your tentacle damage plus 1-1/2 times your Strength
        modifier. While grappling a creature with your tentacle, you are not
        considered grappled and do not take any of the penalties associated with
        that condition. You can maintain this form for 1 minute per Hit Die per
        day; this duration need not be consecutive but must be spent in 1-minute
        increments.</em></p><h2>For Followers of
        Dagon</h2><h3>Feats</h3><p><em>Channel Discord</em></p><h3>Magic Items -
        Wondrous Items</h3><p><em>Dagon's
        Eye</em></p><h3>Traits</h3><p><em>Demonic Persuasion</em></p>
      format: 1
    title:
      level: 1
      show: false
    type: text
sort: 0
