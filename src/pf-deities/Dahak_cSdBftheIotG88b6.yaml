_id: cSdBftheIotG88b6
_key: '!journal!cSdBftheIotG88b6'
_stats:
  coreVersion: '12.331'
name: Dahak
pages:
  - _id: RabsTf4gLZv867ei
    _key: '!journal.pages!cSdBftheIotG88b6.RabsTf4gLZv867ei'
    _stats:
      coreVersion: '12.331'
    name: Dahak
    sort: 0
    text:
      content: >-
        <h1>Dahak</h1><h2>The Endless Destruction</h2>@Source[PZO9290;pages=35]
        <br /><strong>Pathfinder Wiki</strong> <a
        href="http://www.pathfinderwiki.com/wiki/Dahak">Dahak</a><h3>Details</h3><p><strong>Alignment</strong>
        CE<br /><strong>Pantheon</strong> Other Deities<br /><strong>Other
        Pantheons</strong> Kobold Deities, Scalefolk Deities<br /><strong>Areas
        of Concern</strong> Destruction, evil dragons, greed<br
        /><strong>Domains</strong> Chaos, Destruction, Evil, Scalykind,
        Trickery<br /><strong>Subdomains</strong> Catastrophe, Corruption,
        Deception, Demon (Chaos), Demon (Evil), Dragon, Greed, Hatred, Rage,
        Thievery<br /><em>* Requires the <u>Acolyte of Apocrypha</u>
        trait.</em><br /><strong>Favored Weapon</strong> Bite or whip<br
        /><strong>Symbol</strong> Falling burning scale<br /><strong>Sacred
        Animal(s)</strong> None<br /><strong>Sacred Color(s)</strong> Chromatic
        colors</p><h3>Obedience</h3><p>Remain entirely silent for 1 hour while
        you pray to Dahak, ruminating over the glories of wanton destruction
        committed as the culmination of waiting and planning. While you pray,
        let hate and anger fill you until you must lash out. Following your
        prayer, destroy an item worth at least 1 gp and use the shattered
        remains to cut yourself, preferably along preexisting scar lines, while
        espousing your love of chaos and destruction. Such an action deals you 1
        point of damage that cannot be healed for the following 24 hours. You
        gain a +2 profane bonus on Bluff and Intimidate checks against creatures
        who can see your scars.</p><h3>On Golarion</h3><p><strong>Centers of
        Worship</strong> Darklands, the Shackles, Thuvia<br
        /><strong>Nationality</strong> dragon</p><h2>Boons - Deific
        Obedience</h2><h3> Evangelist</h3><p>@Source[PZO9290;pages=35]<br
        /><strong>1: Sorrowmaker's Pact (Sp)</strong> <em>protection from good
         </em> 3/day, <em>align weapon</em> (evil only) 2/day, or <em>rage</em>
        1/day<br /><strong>2: Enemy of Dragonkind (Su)</strong> You can protect
        yourself from the noble forces of the universe and are attuned to
        blocking out draconic threats. When you are under the effects of
        <em>protection from good</em> or a similar effect, true dragons with
        fewer Hit Dice than you count as summoned creatures for the purpose of
        interacting with you, regardless of their alignment. In addition, three
        times per day while under such an effect, you can reroll one saving
        throw against a breath weapon, spell, or spell-like ability used against
        you by a creature of the dragon type. You can only reroll a single
        saving throw once.<br /><strong>3: Bow Before Your End! (Su)</strong>
        You represent one of the most powerful deities in the cosmos, and you
        expect all creatures to recognize the unspeakable power you herald and
        its capacity for wanton destruction. Once per day as a standard action,
        you can create an effect similar to <em>overwhelming
        presence</em><sup>UM</sup> as though you were a cleric with an effective
        caster level equal to half your Hit Dice. Creatures that fail their
        saving throws are staggered instead of helpless for the duration of the
        spell, and suffer no further effects upon a successful subsequent save.
        Creatures of the dragon type must roll their saving throws twice and
        take the lower result. A creature that succeeds at its initial saving
        throw is unaffected by this
        effect.</em></p><h3>Exalted</h3><p>@Source[PZO9290;pages=35]<br
        /><strong>1: Rites of the False Wyrm (Sp)</strong> <em>charm person
         </em> 3/day, <em>mirror image</em> 2/day, or <em>suggestion</em> 1/day<br
        /><strong>2: Draconic Decoys (Su)</strong> Duplicity is second nature to
        you, and you know how to manipulate trickery-based magic to further
        confound your enemies. Anytime you have created an image via <em>mirror
        image</em>, as a swift action you can direct one of your images to move
        to a flanking position against an enemy within 15 feet of you. The image
        allows you or an ally to flank your target for the purpose of a single
        attack. A redirected image dissipates when you or an ally uses the image
        to make a flanking attack, or otherwise remains in place for 1 round
        before dissipating.<br /><strong>3: Flaming Vengeance (Su)</strong> You
        believe that if your enemies are foolish enough to trust you, then they
        are foolish enough to die by Dahak's power. Once per day as a standard
        action, you can gain the benefits of a <em>mislead</em> spell as though
        cast by a wizard of a level equal to your Hit Dice (maximum 20th level),
        with the following modifications. At the time of using this ability, you
        can establish a number of rounds before it will expire, or declare that
        it will expire when your illusory duplicate is touched. When either
        criteria is accomplished, the illusory duplicate explodes as a
        <em>fireball</em> spell cast by a 10th-level
        wizard.</em></p><h3>Sentinel</h3><p>@Source[PZO9290;pages=35]<br
        /><strong>1: Power of the Endless Destruction (Sp)</strong>
        <em>ear-piercing scream<sup>UM</sup> 3/day, <em>shatter</em> 2/day, or
        <em>fireball</em> 1/day<br /><strong>2: Teeth of the Dragon
        (Ex)</strong> Your teeth grow into vicious fangs, akin to those of a
        chromatic dragon, and elemental power issues forth from your mouth in a
        divine gift from Dahak himself. You gain a bite attack. This is a
        primary natural attack that deals 1d6 points of piercing damage for
        Medium creatures or 1d4 points of piercing damage for Small creatures,
        with a critical multiplier of x3. Every time you hit with your bite
        attack, you deal an additional 1d6 points of energy damage (choose from
        acid, cold, electricity, or fire).<br /><strong>3: Elemental Outrage
        (Ex)</strong> You can channel the power of the Endless Destruction
        through your vicious bites-when you hit a creature with your bite
        attack, you can force impressive elemental power into the attack. Up to
        three times per day, when you succeed at a critical hit with your bite
        attack, you deal 5d6 points of energy damage (choose from acid, cold,
        electricity, or fire) instead of 1d6 points. This extra damage is not
        multiplied as part of the critical hit.</em></p><h2>Antipaladin
        Code</h2><em>Antipaladins of Dahak are grim servants of the Endless
        Destruction, their demeanor calm up until the moment their control
        slips. They hunt metallic dragons as a means of repaying Dahak for his
        blessing. Dahak lays down several tenets for his divine warriors to
        follow. <ul><li>I am an instrument of destruction, but the power I wield
        is mine to control, gifted to me by Dahak.</li><li>My wrath is
        ceaseless. It is by my judgment alone to decide when I shall unleash it.
        Once I strike an enemy, I will not stop until they are
        dead.</li><li>None are safe from the rage that boils within me. While
        some may be spared immediate death, I offer no such lenience to the
        metallic dragons, greatest of enemies to the power that gives me
        strength.</li><li>Forgiveness is for the weak. If I am slighted, I will
        go to any lengths to enact my vengeance.</li></ul><h2>For Followers of
        Dahak</h2><h3>Feats</h3><p>Hunter of Dahak</p><h3>Magic Items - Wondrous
        Items</h3><p>Nightstone of Sorrow</p><h3>Traits</h3><p>Dragon Tracker,
        Dragonslayer</p><h2>Unique Spell
        Rules</h2>@Source[PZO9470;pages=28]<p><h3>Antipaladin</h3></p><p><em>Dahak's
        Release</em> can be prepared as a 4th-level
        spell</p><h3>Bloodrager</h3><p><em>Dahak's Release</em> can be prepared
        as a 4th-level spell</p><h3>Cleric/Warpriest</h3><p><em>Dahak's
        Release</em> can be prepared as a 4th-level spell<br /><em>Draconic
        Ally</em> can be prepared as a 3rd-level spell<br /><em>Tail Strike</em>
        can be prepared as a 4th-level
        spell</p><h3>Inquisitor</h3><p><em>Dahak's Release can be prepared as a
        4th-level spell<br /><em>Draconic Ally can be prepared as a</em>
        3rd-level spell<br /><em>Tail Strike can be prepared as a</em> 4th-level
        spell<br /></em></p><h3>Oracle</h3><p><em>Draconic Ally</em> can be
        prepared as a 3rd-level spell<br /><em>Tail Strike</em> can be prepared
        as a 4th-level spell</em></p>
      format: 1
    title:
      level: 1
      show: false
    type: text
sort: 0
