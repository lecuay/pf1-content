_id: PByXvAfEu30oCY6w
_key: '!journal!PByXvAfEu30oCY6w'
_stats:
  coreVersion: '12.331'
folder: Xs7WZeDzdO8Jg0jZ
name: 2.10.07. Damage Reduction
pages:
  - _id: MMAgYNaZfyPWDCzL
    _key: '!journal.pages!PByXvAfEu30oCY6w.MMAgYNaZfyPWDCzL'
    _stats:
      coreVersion: '12.331'
    name: 2.10.07. Damage Reduction
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.OiNttXSDW598DIEs]{(Index) Combat
        Rules}</p><ul><li>@Compendium[pf-content.pf-rules.ArJtyc6VMS8w5R1V]{2.10.01.
        Ability Score
        Bonuses}</li><li>@Compendium[pf-content.pf-rules.njiOxWkkqHk5F5wX]{2.10.02.
        Ability Score Damage, Penalty, and
        Drain}</li><li>@Compendium[pf-content.pf-rules.xpY9oe1ZnDpzXtEK]{2.10.03.
        Afflictions}</li><li>@Compendium[pf-content.pf-rules.8q1lwFFEyleNp24c]{2.10.04.
        Blindsight and
        Blindsense}</li><li>@Compendium[pf-content.pf-rules.Zo8P2hcDTfEW8ful]{2.10.05.
        Channel
        Resistance}</li><li>@Compendium[pf-content.pf-rules.c2QYDs9lJzqZBbaS]{2.10.06.
        Charm and
        Compulsion}</li><li><strong>@Compendium[pf-content.pf-rules.PByXvAfEu30oCY6w]{2.10.07.
        Damage
        Reduction}</strong></li><li>@Compendium[pf-content.pf-rules.sM4GfGeQvrDfIWsI]{2.10.08.
        Darkvision}</li><li>@Compendium[pf-content.pf-rules.FGJ9VUFTKmFBPENt]{2.10.09.
        Death
        Attacks}</li><li>@Compendium[pf-content.pf-rules.PRLXtDrJ7WKN6YzH]{2.10.10.
        Energy Drain and Negative
        Levels}</li><li>@Compendium[pf-content.pf-rules.pJcmOtjOQLzgp5Fr]{2.10.11.
        Energy Immunity and
        Vulnerability}</li><li>@Compendium[pf-content.pf-rules.QD4WRJrKCaJh8Ez3]{2.10.12.
        Energy
        Resistance}</li><li>@Compendium[pf-content.pf-rules.RPid0ZvH9nAN4IgC]{2.10.13.
        Fear}</li><li>@Compendium[pf-content.pf-rules.teI2mSg16BCYCEDS]{2.10.14.
        Invisibility}</li><li>@Compendium[pf-content.pf-rules.FfJv4ZrvXW5kxKb9]{2.10.15.
        Low-Light
        Vision}</li><li>@Compendium[pf-content.pf-rules.F3gWgLEiEIgTpXmd]{2.10.16.
        Paralysis}</li><li>@Compendium[pf-content.pf-rules.ev7xA6qT02JglaS0]{2.10.17.
        Scent}</li><li>@Compendium[pf-content.pf-rules.BGRuRu4kSA0KlT3S]{2.10.18.
        Spell Resistance}</li></ul><hr
        /><p>@Source[PZO1110;pages=561]</p><p>Some magic creatures have the
        supernatural ability to instantly heal damage from weapons or ignore
        blows altogether as though they were invulnerable.</p><p>The numerical
        part of a creature’s damage reduction (or DR) is the amount of damage
        the creature ignores from normal attacks. Usually, a certain type of
        weapon can overcome this reduction (see Overcoming DR). This information
        is separated from the damage reduction number by a slash. For example,
        DR 5/magic means that a creature takes 5 less points of damage from all
        weapons that are not magic. If a dash follows the slash, then the damage
        reduction is effective against any attack that does not ignore damage
        reduction.</p><p>Whenever damage reduction completely negates the damage
        from an attack, it also negates most special effects that accompany the
        attack, such as injury poison, a monk’s stunning, and injury-based
        disease. Damage reduction does not negate touch attacks, energy damage
        dealt along with an attack, or energy drains. Nor does it affect poisons
        or diseases delivered by inhalation, ingestion, or
        contact.</p><p>Attacks that deal no damage because of the target’s
        damage reduction do not disrupt spells.</p><p>Spells, spell-like
        abilities, and energy attacks (even nonmagical fire) ignore damage
        reduction.</p><p>Sometimes damage reduction represents instant healing.
        Sometimes it represents the creature’s tough hide or body. In either
        case, other characters can see that conventional attacks won’t
        work.</p><p>If a creature has damage reduction from more than one
        source, the two forms of damage reduction do not stack. Instead, the
        creature gets the benefit of the best damage reduction in a given
        situation.</p><h2>Overcoming DR</h2><p>Damage reduction may be overcome
        by special materials, magic weapons (any weapon with a +1 or higher
        enhancement bonus, not counting the enhancement from masterwork
        quality), certain types of weapons (such as slashing or bludgeoning),
        and weapons imbued with an alignment.</p><p>Ammunition fired from a
        projectile weapon with an enhancement bonus of +1 or higher is treated
        as a magic weapon for the purpose of overcoming damage reduction.
        Similarly, ammunition fired from a projectile weapon with an alignment
        gains the alignment of that projectile weapon (in addition to any
        alignment it may already have).</p><p>Weapons with an enhancement bonus
        of +3 or greater can ignore some types of damage reduction, regardless
        of their actual material or alignment. The following table shows what
        type of enhancement bonus is needed to overcome some common types of
        damage reduction.</p><table><thead><tr><td>DR Type</td><td>Weapon
        Enhancement Bonus Equivalent</td></tr></thead><tbody><tr><td>Cold
        Iron/Silver</td><td>+3</td></tr><tr><td>Adamantine*</td><td>+4</td></tr><tr><td>Alignment-based</td><td>+5</td></tr></tbody></table><p>*
        Note that this does not give the ability to ignore hardness, like an
        actual adamantine weapon does</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
