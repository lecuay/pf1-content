_id: qYmTleYgnpLOu7gq
_key: '!journal!qYmTleYgnpLOu7gq'
_stats:
  coreVersion: '12.331'
folder: B3pncms1TJP9sIoG
name: 3.5.3.2.1. Positive Energy Plane
pages:
  - _id: gIp21strJzY9v4rY
    _key: '!journal.pages!qYmTleYgnpLOu7gq.gIp21strJzY9v4rY'
    _stats:
      coreVersion: '12.331'
    name: 3.5.3.2.1. Positive Energy Plane
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.XOJpjqmBzwE4R4n0]{(Index)
        Environment}</p><p>@Compendium[pf-content.pf-rules.ryIRK7CjUk9fI8TT]{3.5.
        The
        Planes}</p><ul><li><p>@Compendium[pf-content.pf-rules.EQFsfwmMHxeX34rq]{3.5.3.2.
        Energy
        Planes}</p><ul><li><strong>@Compendium[pf-content.pf-rules.qYmTleYgnpLOu7gq]{3.5.3.2.1.
        Positive Energy
        Plane}</strong></li><li>@Compendium[pf-content.pf-rules.wlkXoMNImMqgBRmK]{3.5.3.2.2.
        Negative Energy Plane}</li></ul></li></ul><hr
        /><p>@Source[PZO1114;pages=188]</p><p>The Positive Energy Plane has no
        surface and is akin to the Plane of Air with its wide-open nature.
        However, every bit of this plane glows brightly with innate power. This
        power is dangerous to mortal forms, which are not made to handle it.
        Despite the beneficial effects of the plane, it is one of the most
        hostile of the Inner Planes. An unprotected character on this plane
        swells with power as positive energy is forced upon her. Then, because
        her mortal frame is unable to contain that power, she is immolated, like
        a mote of dust caught at the edge of a supernova. Visits to the Positive
        Energy Plane are brief, and even then travelers must be heavily
        protected.</p><h2>Planar Traits</h2><p>The Positive Energy Plane has the
        following traits:</p><ul><li><strong>Subjective Directional
        Gravity</strong></li><li><strong>Major Positive-Dominant</strong>: Some
        regions of the plane have the minor positive-dominant trait instead, and
        those islands tend to be inhabited.</li><li><strong>Enhanced
        Magic</strong>: Spells and spell-like abilities that use positive energy
        are enhanced. Class abilities that use positive energy, such as channel
        positive energy, gain a +4 bonus to the save DC to resist the
        ability.</li><li><strong>Impeded Magic</strong>: Spells and spell-like
        abilities that use negative energy (including inflict spells) are
        impeded.</li></ul><h2>Description</h2><p>@Source[PZO1132;pages=239]</p><p>The
        Positive Energy Plane is the source of all life, the Cosmic Fire at the
        heart of the multiverse that gives birth to mortal souls. The plane has
        no surface and exists as an emanation of life-giving energy radiating
        from an incandescent interior that resembles the molten heart of an
        active star. Ironically for a plane associated with life, the Positive
        Energy Plane can be extremely deadly to mortal visitors, as its ambient
        energies are so powerful that a mortal shell cannot absorb them without
        bursting. At certain vertices, the refraction of the Cosmic Fire’s rays
        create islands of solidity where the plane’s energies are not so
        extreme, and some manner of life as mortals understand it becomes
        possible. Here, upon vast shimmering fields, phoenix-feathered creatures
        known as the jyoti tend to orchards of glowing, anemone-like trees as
        tall as mountains, sprouting immature souls like glossy, liquid fruit.
        The xenophobic jyoti dwell in complex cities of crystal specially
        designed to reflect the weird luminescence of the Cosmic Fire. Jyoti
        seldom venture from these structures, focusing all of their energies on
        their sacred charge of tending and defending the nascent souls of the
        multiverse. At the center of each jyoti city is an imposing gate to a
        star in the cosmos of the Material Plane. New souls pass through these
        gates and ride waves of light to find incarnation in mortal
        vessels.</p><p>If the jyoti dedicate themselves to the protection of
        incubating immature souls, the other primary inhabitants of the Positive
        Energy Plane, the manasaputras, dedicate their existence to assisting
        the spiritual development of mortals. These “sons of mind” are the
        powerful psychic incarnations of mortals who have endured scores of
        mortal reincarnations, with each step becoming more attuned to the
        universal undersoul. The greatest and most powerful of the
        manasaputras—the glory-clad solar kumaras— dwell within the heart of the
        Cosmic Fire, and claim to be in communication with it. Lesser
        manasaputras like agnishvattas, barhisads, and manus spread through the
        Inner Sphere to initiate mortal adepts in the occult nature of the
        multiverse, so that they too might step once again into the light that
        birthed them.</p><p>For reasons unknown even to the eldest natives,
        divine beings cannot enter the Positive Energy Plane. Refugees from the
        vengeance of the gods or those hoping to hide important relics from
        certain divinities sometimes venture to the Positive Energy Plane to
        negotiate with the jyoti, who over the centuries have amassed an
        astounding trove of world-shattering artifacts, illegitimate half-mortal
        bastards, heretics, and other dangers.</p><p>The Positive Energy Plane
        is a luminous, burning void and the origin of pre-incarnate souls—the
        source of all life.</p><p>The Positive Energy Plane is the source of
        pre-incarnate mortal souls and the wellspring of life’s animating force,
        but despite this, it may well be the single most hostile plane of
        existence. Blinding, brilliant, and burning, Creation’s Forge resembles
        the heart of a blazing star. There is precious little solid ground
        except for outcroppings of iridescent crystal composed of solid positive
        energy, and despite the popular but incorrect association of positive
        energy with goodness, the force of unrestrained life is relentlessly
        destructive to mortal flesh.</p><p>The bright twin of the Negative
        Energy Plane’s entropic force, the Positive Energy Plane receives raw
        quintessence devoured by Limbo, forming one pole of the cosmic cycle of
        souls. It is here that souls originate and begin their journey, in this
        place paradoxically inimical to most life, often comparatively barren
        and sterile, but nevertheless having an overwhelming alien
        beauty.</p><h2>Divinities</h2><p>Among the planes, the Positive Energy
        Plane is unusual not merely due to the lack of deities who make their
        domain there but because deities are entirely barred from entry and
        taking direct action in the realm. Whether this is a self-imposed
        limitation or something of a fundamental truth of reality is unclear.
        Among demigods, only kumaras—incredibly powerful manasaputras—maintain
        realms here, and they are quick to explain, when asked, that they are
        visitors to Creation’s Forge; that such visits might last for thousands
        upon thousands of years is somewhat irrelevant to an ageless entity.
        Other demigods avoid the plane entirely. Servants of the divine face no
        such restriction, of course.</p><p>The deep antipathy and suspicion
        jyoti harbor toward the divine certainly suggests that their race’s
        existence has some unknown link to whatever bars deities from entering
        the Positive Energy Plane. Some scholars have inferred their own
        reasons, suggesting that the presence of shackled danavas within the
        plane speaks to an early attempt by a deity (or maybe even an entire
        pantheon) to interfere with or regulate the flow of pre-incarnate mortal
        souls, a concept jyoti find abhorrent. Additional sources (primarily
        those associated with the Horsemen of the Apocalypse—although curiously
        enough, not in the infamous pages of the Book of the Damned) refer to
        the Positive Energy Plane as “the feast that eludes,” suggesting early
        attempts by the Horsemen to investigate the source of mortal souls and
        potentially short-circuit the system in order to accomplish their goal
        of ending all mortal life.</p><h2>Denizens</h2><p>The greatest danger of
        the Positive Energy Plane is the nature of the plane itself, but those
        who manage to protect themselves from being overwhelmed by raw life
        force and the blinding vistas can find much to wonder and marvel at
        within Creation’s Forge. There are no astronomical bodies to speak of in
        the “skies” of this plane, nor is there a workable analog to
        north.</p><p>That fewer beings dwell upon the Positive Energy Plane than
        the Negative Energy Plane is one of the oldest ironies of the Great
        Beyond. At least on the Negative Energy Plane, undead can dwell and
        prosper—but on the Positive Energy Plane, the opposite does not hold
        true. Living creatures are bolstered and energized by brief exposure to
        the Positive Energy Plane, but those who stay too long are quickly
        overloaded by raw life force and perish as their souls are absorbed and
        recycled. Despite this inhospitable feature, a few forms of life have
        managed to adapt and even flourish in this overwhelming
        plane.</p><h3>Danavas</h3><p>The first danavas came to the Positive
        Energy Plane near the dawn of creation itself, but though they are among
        reality’s oldest denizens, today they are met with abhorrence and hatred
        by other natives of the plane. Jyoti view them as a grotesque meddling
        by the gods in a task that was removed from deific purview and instead
        granted to jyoti by the nameless presence of their own convoluted
        religion, which remains opaque to those outside their race. The presence
        of those few danavas in Creation’s Forge, and in the Void as well, may
        in fact be what sparked jyoti’s antipathy towards the
        divine.</p><h3>Jyoti</h3><p>Often erroneously called phoenix-kin, jyoti
        are the most populous natives of the Positive Energy Plane. They appear
        as radiant humanoid birds with glowing auras, but unlike phoenixes,
        which are benevolent magical beasts, jyoti are cold, secretive,
        exceptionally xenophobic, and have a severe antipathy and distrust of
        the divine. Dwelling in towering cities grown of luminous, living
        crystal, jyoti are guardians and stewards of their plane, and they see
        themselves as protectors and even cultivators of the Furnace’s
        preincarnate souls. Who or what appointed them is unknown— the
        “appointment” may indeed simply be a perpetuation of their own
        perceptions of their kind’s importance in the Great Beyond. Jyoti are
        not born, nor do they age. They form spontaneously from their plane’s
        raw and burning essence, and their subsequent activities help to keep
        the cosmic cycle of souls flowing. More than anything, jyoti abhor
        negative energy and those who use it, whether directly or indirectly (as
        in the case of undead). Yet rather than being an antipathy borne of
        rival energies, jyoti’s relationship to sceaduinars on the Negative
        Energy Plane is strangely complicated; scholars suspect a deep and
        opaque history exists between the races. Jyoti view sceaduinars not as
        enemies, but as objects of profound pity, and they embrace the act of
        destroying them as a mercy.</p><h3>Manasaputras</h3><p>Manasaputras
        manifest when the reincarnated soul of a mortal who sought perfection
        and spiritual transcendence in life merges with the raw energy of life
        itself. Shedding the bonds of mortality, these souls continue on in
        their new forms, seeking to guide other mortals through the same
        difficult trek towards enlightenment. Manasaputras share a mutual
        respect with jyoti, who temper their instinctive xenophobia when they
        meet (though neither do jyoti actively seek out manasaputras to interact
        with). Manasaputras welcome visitors, and their acceptance and
        hospitality typically is accompanied by lessons geared toward the
        visitor’s spiritual maturation and enlightenment. The greatest
        manasaputras seek out the wisdom of a mysterious presence they call the
        Logos—a presence some equate with the same ill-defined object of the
        aeons’ worship and perhaps also that which the jyoti claim tasked them
        with guardianship of souls. of the three, only the manasaputras ever
        discuss the subject with others, but their teachings come in the form of
        personal quests geared toward guiding scholars to come to their own
        conclusions, rather than simply imparting
        answers.</p><h3>Turuls</h3><p>The relationship these gargantuan and
        powerful avian outsiders have with jyoti is a curious one indeed—a
        relationship that certainly goes beyond mere physical similarity. Some
        believe the two races to be different incarnations of the same genesis,
        with others going a step further to suggest turuls represent a higher
        form of reincarnation than jyoti. Neither turuls nor jyoti speak of the
        matter. Often, a turul can be found dwelling among jyoti, living
        alongside them atop great spires of crystal and acting as protectors and
        advisors even though jyoti tend to ignore their presence. More often,
        however, turuls prefer isolation. They often watch over places where the
        boundaries between the Material Plane and the Positive Energy Plane wear
        thin, roosting amid great crystalline trees or vast mountains of force.
        While moody and haughty, turuls often select mortal communities and pose
        as divine figures or beings of prophecy, working to manipulate and
        shepherd Material Plane societies toward mysterious goals.</p><h2>Magic
        on the Positive Energy Plane</h2><p>Spells and spell-like abilities that
        use positive energy (including cure spells) are enhanced. Class
        abilities that use positive energy, such as channel energy, gain a +4
        bonus to the save DC to resist the ability.</p><p>Spells and spell-like
        abilities that use negative energy are impeded.</p><h2>Darkness and
        Light</h2><p>The process by which pre-incarnate souls migrate from the
        Positive Energy Plane into the Material Plane is not well understood;
        likewise, the point at which a soul infuses a life is unknown and
        subject to great debate across all worlds.</p><p>One particularly
        compelling theory holds that at the heart of every star in the universe
        lies a pinpoint portal to the Positive Energy Plane. Whether these
        portals are what ignites new stars, or whether they form as a side
        effect of the cosmic violence of a new star’s explosive birth is
        unclear, and the evidence gives little weight to one side over the
        other. If stars are the points of access for new souls into the Material
        Plane, however, this may explain why the Material Plane is the only
        plane in the entire Great Beyond that is not inhabited primarily by
        outsiders.</p><p>Some scholars have taken this theory a step further and
        have proposed that black holes are similar pinpoint portals, connecting
        instead to the Negative Energy Plane. How the proximity of a black hole
        to a world might affect the paths taken by souls into the River of Souls
        is unknown, since such regions of space typically leave nothing behind
        to learn from—black holes consume everything, after
        all!</p><h2>Pre-Incarnate Souls</h2><p>Pre-incarnate souls have no
        sentience and are more akin to a form of energy than to distinct
        creatures, as it is the process of life itself that shapes a
        pre-incarnate soul into an actual soul. At most, a pre-incarnate souls
        act like natural forces with a minor tendency to mirror the actions of
        living creatures, moving and flowing with a semblance of life and
        organization, though this is little more than an illusion.</p><p>New
        pre-incarnate souls are constantly manifesting, and are almost
        impossible to destroy. Powerful blasts of negative energy could wipe out
        a pre-incarnate soul, as could magic like wish or miracle, but not
        effects that directly target and damage souls, since a pre-incarnate
        soul lacks the qualities that allow, for example, an astradaemon’s
        devour soul ability or trap the soul to function. Despite these facts,
        jyoti are ferociously protective of pre-incarnate souls and act like
        shepherds where they gather, nurturing and singing to them as they
        slowly mature during their migration toward life on the Material
        Plane.</p><p>A pre-incarnate soul is a quivering, floating mass of
        quintessence, spiritual potential, and positive energy, and contact with
        one can have unusual results. A character who comes into contact with a
        pre-incarnate soul increases the effects of positive-dominant essence
        traits currently affecting the character by one step (or doubles the
        effects of major positive-dominant effects) for as long as the contact
        persists (minimum of 1 round). In addition, each round of contact with a
        pre-incarnate soul removes 1 negative level from a creature per
        round.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
