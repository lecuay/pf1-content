_id: PXvEMec4O2tNy1Hy
_key: '!journal!PXvEMec4O2tNy1Hy'
_stats:
  coreVersion: '12.331'
folder: CMTuXqZ6Ywxqk9Ar
name: 7.1.3.3. Dealing with Outsiders
pages:
  - _id: wpzJ1MSJjk0efYsY
    _key: '!journal.pages!PXvEMec4O2tNy1Hy.wpzJ1MSJjk0efYsY'
    _stats:
      coreVersion: '12.331'
    name: 7.1.3.3. Dealing with Outsiders
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.dS0UkKOINqI2iTDX]{(Index)
        Magic}</p><p>@Compendium[pf-content.pf-rules.zCeqoMSKdZ8r4EPd]{7.1.
        Mastering
        Magic}</p><ul><li><p>@Compendium[pf-content.pf-rules.Sz0GZaZV3zxb9UTY]{7.1.3.
        Binding
        Outsiders}</p><ul><li>@Compendium[pf-content.pf-rules.g5Sh1IZvbdRSe52K]{7.1.3.1.
        Calling
        Outsiders}</li><li>@Compendium[pf-content.pf-rules.lm8G3ycvWUhavcRt]{7.1.3.2.
        True
        Names}</li><li><strong>@Compendium[pf-content.pf-rules.PXvEMec4O2tNy1Hy]{7.1.3.3.
        Dealing with
        Outsiders}</strong></li><li>@Compendium[pf-content.pf-rules.M5p2I9If5lZHg5iI]{7.1.3.4.
        Outsider Categories}</li></ul></li></ul><hr
        /><p>@Source[PZO1117;pages=102]</p><p>All outsiders love that which
        makes them strong. They seek to promote those qualities that offer them
        the greatest power, and covet their own survival. As beings—some might
        even call them concepts—of thought, will, and power, outsiders reward
        those who help them make their core concepts immortal.</p><p>In
        short:</p><ul><li>Aeons are dedicated to their often obscure and
        contradictory goals.</li><li>Agathions love the defense of good without
        regard for law and chaos.</li><li>Angels love beauty and things that
        destroy evil.</li><li>Archons love pure souls and order.</li><li>Azatas
        love beauty and freedom.</li><li>Daemons love death and
        oblivion.</li><li>Demons love suffering.</li><li>Devils love souls of
        any sort.</li><li>Elementals love power.</li><li>Inevitables and
        axiomites hate chaos and are focused on their goals.</li><li>Proteans
        love chaos and want to return the multiverse to its original chaotic
        state.</li><li>Qlippoth hate all intelligent life, as it is the engine
        of sin, and want it destroyed.</li></ul><p>The reward outsiders offer
        may be actual aid, grudging service, or even just agreeing not to devour
        the binder’s soul. Regardless, it is always—always—in the binder’s best
        interest to make the summoning as painless as possible for the target,
        or else to overawe the summoned creature with the threat of utter
        destruction or millennia of endless pain. Attempting to treat outsiders
        as equals and the pact as a mere negotiating tool almost always ends in
        disaster. More specifics for each type of outsider are described
        below.</p><p>Offering appropriate gifts to the summoned creature can
        provide the caster a +2 bonus on the opposed Charisma check to keep it
        on the Material Plane. Indeed, if the gift is sweet enough, the outsider
        may choose not to break the strictures of the summoning, even if it has
        the opportunity to do so. All gifts, whether or not they are good enough
        to please the outsider, disappear at the spell’s conclusion. Only the
        worst sorts of gifts are rejected; such a rejection indicates that the
        summoned creature feels gravely insulted.</p><h1>Anathematic
        Substances</h1><p>@Source[PZO1117;pages=102]</p><p>All outsiders have
        vulnerabilities, and those who deal with them must know what these
        vulnerabilities are. Some binders even use weapons composed of
        anathematic substances to create or draw their magic circles, or may
        even grind such valuable weapons up to create the powder to make the
        circles.</p><p>For every 5,000 gp of an anathematic substance used, the
        caster gains a +1 bonus on the opposed Charisma check to bargain with
        the outsider. This destroys the
        substance.</p><h2>Anarchic</h2><p>Infused with the power of chaos,
        anarchic weapons are anathema to many lawful outsiders, even those who
        are not specifically vulnerable to the
        weapons.</p><h2>Axiomatic</h2><p>Empowered by law, axiomatic weapons are
        harmful to chaotic outsiders, dealing extra damage even if the outsider
        is not particularly vulnerable to its effect.</p><h2>Alchemical
        Silver</h2><p>While a weapon made of alchemical silver reduces damage by
        1, with a minimum of 1 point of damage, it may be more effective than
        other weapons against certain outsiders. It has 10 hit points per inch
        of thickness and hardness 8.</p><h2>Cold Iron</h2><p>Effective against
        daemons, demons, and fey, cold iron has been drawn from deep beneath
        forbidding mountains and forged with the least heat possible. Because of
        the delicacy and difficulty of the process, a weapon made of cold iron
        costs twice as much to make, and every magical enhancement increases its
        price significantly. It has 30 hit points per inch of thickness and
        hardness 10.</p><h2>Holy</h2><p>A holy weapon is any weapon imbued with
        holy power, which allows it to bypass damage reduction for specific evil
        creatures and inflict an additional 2d6 points of damage on those
        monsters. Evil outsiders that do not have a specific vulnerability to
        holy weapons still take that additional damage if the weapon overcomes
        the creature’s damage reduction.</p><h2>Mithral</h2><p>Most outsiders
        react to mithral in the same way that they do to actual
        silver.</p><h2>Silver</h2><p>Long revered for its purity and ability to
        harm lycanthropes and devils, silver is also used to trap certain kinds
        of good outsiders.</p><h2>Unholy</h2><p>The opposite of the holy weapon,
        an unholy weapon inflicts its damage on good-aligned outsiders, but is
        in other respects the same.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
