_id: wb9tYgBlWX8peqFl
_key: '!journal!wb9tYgBlWX8peqFl'
_stats:
  coreVersion: '12.331'
folder: CMTuXqZ6Ywxqk9Ar
name: 7.1.5.14. Components
pages:
  - _id: W3xdajlK2mR3ivMd
    _key: '!journal.pages!wb9tYgBlWX8peqFl.W3xdajlK2mR3ivMd'
    _stats:
      coreVersion: '12.331'
    name: 7.1.5.14. Components
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.dS0UkKOINqI2iTDX]{(Index)
        Magic}</p><p>@Compendium[pf-content.pf-rules.zCeqoMSKdZ8r4EPd]{7.1.
        Mastering
        Magic}</p><ul><li><p>@Compendium[pf-content.pf-rules.xdyGkidQGN7vYL1E]{7.1.5.
        Designing
        Spells}</p><ul><li>@Compendium[pf-content.pf-rules.7Lc81tg2g1Q6TMPo]{7.1.5.01.
        The Golden
        Rule}</li><li>@Compendium[pf-content.pf-rules.DZBAa9axA5Gv4el0]{7.1.5.02.
        Spell
        Terminology}</li><li>@Compendium[pf-content.pf-rules.dbb5pwAitIa3cqqj]{7.1.5.03.
        Intended
        Level}</li><li>@Compendium[pf-content.pf-rules.1DkXvcE3KMmBww9Z]{7.1.5.04.
        Function}</li><li>@Compendium[pf-content.pf-rules.Xu1jyIRMERPFh8X6]{7.1.5.05.
        Spell
        Research}</li><li>@Compendium[pf-content.pf-rules.fF9HpImlhp8oNX5Q]{7.1.5.06.
        Spell
        Damage}</li><li>@Compendium[pf-content.pf-rules.zrMFTIHfzbQTTwZ1]{7.1.5.07.
        Target}</li><li>@Compendium[pf-content.pf-rules.wsF1rdV2ACkYU3Su]{7.1.5.08.
        Damage
        Caps}</li><li>@Compendium[pf-content.pf-rules.9TyMr3XedNUcWVzB]{7.1.5.09.
        Range}</li><li>@Compendium[pf-content.pf-rules.KlR2PDab6uknvopJ]{7.1.5.10.
        Duration}</li><li>@Compendium[pf-content.pf-rules.x0E7IZ5imhQ5srrf]{7.1.5.11.
        Saving
        Throw}</li><li>@Compendium[pf-content.pf-rules.RRG1A0rBL44Qeskg]{7.1.5.12.
        Spell
        Resistance}</li><li>@Compendium[pf-content.pf-rules.kvl8JOILza5HEj7N]{7.1.5.13.
        Casting
        Time}</li><li><strong>@Compendium[pf-content.pf-rules.wb9tYgBlWX8peqFl]{7.1.5.14.
        Components}</strong></li><li>@Compendium[pf-content.pf-rules.PCvwd6PaQH7mD7o6]{7.1.5.15.
        School}</li><li>@Compendium[pf-content.pf-rules.dmUzb6b2BspbBdDp]{7.1.5.16.
        Bonus
        Type}</li><li>@Compendium[pf-content.pf-rules.gh6sxSeK1RNLLFoN]{7.1.5.17.
        Description}</li><li>@Compendium[pf-content.pf-rules.q6Xva2npOy7fVL42]{7.1.5.18.
        Hierarchy of Attack
        Effects}</li><li>@Compendium[pf-content.pf-rules.s6CWKZDo0zQeRXMJ]{7.1.5.19.
        Depletable
        Statistics}</li><li>@Compendium[pf-content.pf-rules.r8zYCFuq59NEtjkz]{7.1.5.20.
        Core is
        King}</li><li>@Compendium[pf-content.pf-rules.aVUL2wrJszAUzNMK]{7.1.5.21.
        Multipurpose
        Spells}</li><li>@Compendium[pf-content.pf-rules.Ak9Ws3Z16GY6QSoH]{7.1.5.22.
        Choosing
        Descriptors}</li><li>@Compendium[pf-content.pf-rules.baSlaM24kwB68nuf]{7.1.5.23.
        Benchmarks}</li></ul></li></ul><hr
        /><p>@Source[PZO1117;pages=133]</p><p>For the most part, a spell’s
        components have very little to do with its overall power level unless it
        requires a costly focus or material component or has no component at
        all. Most spells in the Core Rulebook have verbal and somatic
        components, and new spells should follow this trend.</p><p>The advantage
        of spells that don’t require verbal components is they can be cast in an
        area of silence, and thus there is the temptation to create silent
        versions of common combat spells. However, doing so devalues the Silent
        Spell feat, just like making swift-action spells devalues Quicken Spell,
        though not to such a great extent (casting two spells per round is a
        more serious problem than having a backup spell to counteract an
        unexpected silence). If casters decide they’d rather prepare a silent
        magic missile instead of acid arrow, or a silent acid arrow instead of
        fireball, they’ve deliberately chosen weaker options, and that’s
        fine.</p><p>The advantage of spells that don’t require somatic
        components is they can be cast when bound, grappled, or when both hands
        are full or occupied, and arcane spell failure doesn’t apply. Just as
        creating silent versions of spells devalues Silent Spell, making
        non-somatic spells devalues the Still Spell feat. The premise of the
        game is that most spells require words and gestures, and new spells
        should stick with that unless the theme of the spell suggests it
        wouldn’t require a somatic component, or it was specifically designed to
        escape bindings or grapples.</p><p>The advantage of spells that don’t
        require material components is they don’t require a spell component
        pouch (and in the rare circumstance in which if you’re grappled, you
        needn’t already have your material components in hand to cast the
        spell). Most material components are part of a spell for flavor rather
        than to satisfy rules. The guano and sulfur material components of
        fireball are there because early gunpowder (black powder) was made from
        guano and sulfur. The fur and glass rod material components of lightning
        bolt come from the ability to create a buildup of static electricity by
        rubbing fur against a glass rod. The game could present those spells
        without material components at all, and it would have a negligible
        effect on how the game plays (as proven by the “it has whatever I need”
        spell component pouch, and the sorcerer class getting Eschew Materials
        as a bonus feat)—they’re just in the spell for fun. Balance your spell
        assuming it has no material components or free material components, and
        then add them in if the flavor seems appropriate.</p><p>Costly material
        components should be used to prevent overzealous players from casting
        the spell as often as they want, because the spell either makes
        adventuring too easy if everyone in the party has it (such as
        stoneskin), allows the PCs to bypass key adventuring experiences like
        exploring and investigating (such as augury, divination, and commune),
        or allows the PCs to trivialize certain threats (such as raise dead and
        restoration). Balance a spell without costly material components if
        possible, usually by raising the spell level if it is too good for the
        intended level. Sometimes the power level of a spell is on target (like
        augury, as it makes sense to have a low-level divination spell for
        clerics), but the spell is valuable enough that players will overuse it
        if it’s free, so you have to apply a gp cost to moderate how often the
        PCs use it. Long-lasting defensive spells such as glyph of warding also
        fit into this category; if they were free, every spellcaster would cover
        her lair in them, casting one per day for the weeks or months of
        planning the NPC has before the PCs arrive. By giving glyph of warding a
        gp cost, it allows for more traditional adventuring—otherwise every
        square the PCs walk on is a potential trap, slowing play to a crawl as
        the PCs are forced to slowly and carefully search every square to notice
        the glyphs (given that a typical 5th-level rogue has +14 to Perception
        against a DC 28 glyph, meaning she fails most searches unless she takes
        20).</p><p>Focus components are governed by the same rules as material
        components—in most cases they’re just there for flavor, and are only
        relevant if costly. A costly focus is like a costly material component,
        except it’s a one-time expenditure rather than a repeat expenditure, a
        barrier to entry that you can ignore once you’ve crossed the threshold.
        A costly focus is a good way to delay when PCs gain access to the spell,
        but once they have the materials, it’s essentially just like any other
        spell without a costly focus. As with material components, balance the
        spell for its level, and if it seems like the spell is too good and
        delaying access to it would help moderate it, consider adding a costly
        focus component.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
