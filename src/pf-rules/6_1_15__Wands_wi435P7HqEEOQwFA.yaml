_id: wi435P7HqEEOQwFA
_key: '!journal!wi435P7HqEEOQwFA'
_stats:
  coreVersion: '12.331'
folder: 1SBSobU6ZnbSTMmG
name: 6.1.15. Wands
pages:
  - _id: ymNZO5E9LAir5v6n
    _key: '!journal.pages!wi435P7HqEEOQwFA.ymNZO5E9LAir5v6n'
    _stats:
      coreVersion: '12.331'
    name: 6.1.15. Wands
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.4TX2Q1MaWt1B8298]{(Index)
        Items}</p><p>@Compendium[pf-content.pf-rules.s3ene84nkQAyhCrW]{6.1.
        Magic
        Items}</p><ul><li>@Compendium[pf-content.pf-rules.sP6qrGJREE0X596I]{6.1.01.
        Magic Items and Detect
        Magic}</li><li>@Compendium[pf-content.pf-rules.N3qNOnemt2nuACdl]{6.1.02.
        Using
        Items}</li><li>@Compendium[pf-content.pf-rules.VOtMqlqVoaZs7zu3]{6.1.03.
        Magic Items on the
        Body}</li><li>@Compendium[pf-content.pf-rules.EyKVuz8Ae2Bh5Z32]{6.1.04.
        Saving Throws Against Magic Item
        Powers}</li><li>@Compendium[pf-content.pf-rules.oXM5hPY9YSNGCEpU]{6.1.05.
        Damaging Magic
        Items}</li><li>@Compendium[pf-content.pf-rules.M3XyBvOcp11YAnAL]{6.1.06.
        Purchasing Magic
        Items}</li><li>@Compendium[pf-content.pf-rules.iZ6tZ7PFna0TiXkW]{6.1.07.
        Magic Item
        Descriptions}</li><li>@Compendium[pf-content.pf-rules.b91t5OrtW3axrmKd]{6.1.08.
        Magic
        Armor}</li><li>@Compendium[pf-content.pf-rules.TT91XHZCetT5yyVc]{6.1.09.
        Magic
        Weapons}</li><li>@Compendium[pf-content.pf-rules.itDodwwkCVnIf0jU]{6.1.10.
        Potions}</li><li>@Compendium[pf-content.pf-rules.LFs1l4pTdQgqrkjx]{6.1.11.
        Rings}</li><li>@Compendium[pf-content.pf-rules.LxBkrhoOTfPeNQ7Z]{6.1.12.
        Rods}</li><li>@Compendium[pf-content.pf-rules.2QvAhr3WucxaQNeN]{6.1.13.
        Scrolls}</li><li>@Compendium[pf-content.pf-rules.3mXUlZodNKkPDj0D]{6.1.14.
        Staves}</li><li><strong>@Compendium[pf-content.pf-rules.wi435P7HqEEOQwFA]{6.1.15.
        Wands}</strong></li><li>@Compendium[pf-content.pf-rules.HYNbOvjpMu4OZlfs]{6.1.16.
        Wondrous
        Items}</li><li>@Compendium[pf-content.pf-rules.muHuo2GwcCKyy4tP]{6.1.17.
        Intelligent
        Items}</li><li>@Compendium[pf-content.pf-rules.LaZ4cSkdCxMwDnXD]{6.1.18.
        Cursed
        Items}</li><li>@Compendium[pf-content.pf-rules.87rVetLgCkc8cm5d]{6.1.19.
        Artifacts}</li><li>@Compendium[pf-content.pf-rules.X6hkpqXjE3T3jRy2]{6.1.20.
        Magic Item Creation}</li></ul><hr
        /><p>@Source[PZO1110;pages=496]</p><p>A wand is a thin baton that
        contains a single spell of 4th level or lower. A wand has 50 charges
        when created—each charge allows the use of the wand’s spell one time. A
        wand that runs out of charges is just a stick. The price of a wand is
        equal to the level of the spell × the creator’s caster level × 750 gp.
        If the wand has a material component cost, it is added to the base price
        and cost to create once for each charge (50 × material component cost).
        Table 15–17 gives sample prices for wands created at the lowest possible
        caster level for each spellcasting class. Note that some spells appear
        at different levels for different casters. The level of such spells
        depends on the caster crafting the wand.</p><h2>Physical
        Description</h2><p>A wand is 6 to 12 inches long, 1/4 inch thick, and
        weighs no more than 1 ounce. Most wands are wood, but some are bone,
        metal, or even crystal. A typical wand has AC 7, 5 hit points, hardness
        5, and a break DC of 16.</p><h2>Activation</h2><p>Wands use the spell
        trigger activation method, so casting a spell from a wand is usually a
        standard action that doesn’t provoke attacks of opportunity. (If the
        spell being cast has a longer casting time than 1 action, however, it
        takes that long to cast the spell from a wand.) To activate a wand, a
        character must hold it in hand (or whatever passes for a hand, for
        nonhumanoid creatures) and point it in the general direction of the
        target or area. A wand may be used while grappling or while swallowed
        whole.</p><h2>Special Qualities</h2><p>Roll d%. A 01–30 result indicates
        that something (a design, inscription, or the like) provides some clue
        to the wand’s function, and 31–100 indicates no special
        qualities.</p><h2>Table 15-17:
        Wands</h2><table><thead><tr><td>Minor</td><td>Medium</td><td>Major</td><td>Spell
        Level</td><td>Caster
        Level</td></tr></thead><tbody><tr><td>01-05</td><td>—</td><td>—</td><td>0</td><td>1st</td></tr><tr><td>06-60</td><td>—</td><td>—</td><td>1st</td><td>1st</td></tr><tr><td>61-100</td><td>01-606</td><td>—</td><td>2nd</td><td>3rd</td></tr><tr><td>—</td><td>61-100</td><td>01-60</td><td>3rd</td><td>5th</td></tr><tr><td>—</td><td>—</td><td>61-100</td><td>4th</td><td>7th</td></tr></tbody></table><hr
        /><p>@Compendium[pf1.ultimate-equipment.TeMae6GRt7G53JVJ]{Common Level 0
        Wands} @Compendium[pf1.ultimate-equipment.VKYvNo9t1wQ3pCEk]{Uncommon
        Level 0
        Wands}</p><p>@Compendium[pf1.ultimate-equipment.Kxtzw02nxm5GWPb9]{Common
        Level 1 Wands}
        @Compendium[pf1.ultimate-equipment.2UQtiEKX67vy7gUT]{Uncommon Level 1
        Wands}</p><p>@Compendium[pf1.ultimate-equipment.qeC9fgOka2MItT2A]{Common
        Level 2 Wands}
        @Compendium[pf1.ultimate-equipment.visy19CsXY2jpABp]{Uncommon Level 2
        Wands}</p><p>@Compendium[pf1.ultimate-equipment.dB3WvGIjmNmuI0kP]{Common
        Level 3 Wands}
        @Compendium[pf1.ultimate-equipment.tQ3dzkGqsrzhzHKl]{Uncommon Level 3
        Wands}</p><p>@Compendium[pf1.ultimate-equipment.tZRMfjJerg5a9O5N]{Common
        Level 4 Wands}
        @Compendium[pf1.ultimate-equipment.AoVoLFKJpX4bPUqh]{Uncommon Level 4
        Wands}</p><hr /><h2>Wand Costs</h2><table
        border="1"><thead><tr><td>Spell Level</td><td>Cleric, Druid,
        Wizard</td><td>Sorcerer</td><td>Bard</td><td>Paladin,
        Ranger</td></tr></thead><tbody><tr><td>0</td><td>375 gp</td><td>375
        gp</td><td>375 gp</td><td>—</td></tr><tr><td>1st</td><td>750
        gp</td><td>750 gp</td><td>750 gp</td><td>750
        gp</td></tr><tr><td>2nd</td><td>4,500 gp</td><td>6,000 gp</td><td>6,000
        gp</td><td>6,000 gp</td></tr><tr><td>3rd</td><td>11,250
        gp</td><td>13,500 gp</td><td>15,750 gp</td><td>15,750
        gp</td></tr><tr><td>4th</td><td>21,000 gp</td><td>24,000
        gp</td><td>30,000 gp</td><td>30,000 gp</td></tr></tbody></table>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
