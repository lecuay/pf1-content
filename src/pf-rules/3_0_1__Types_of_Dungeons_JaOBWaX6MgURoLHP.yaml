_id: JaOBWaX6MgURoLHP
_key: '!journal!JaOBWaX6MgURoLHP'
_stats:
  coreVersion: '12.331'
folder: UW4tzDX1A62qPAzr
name: 3.0.1. Types of Dungeons
pages:
  - _id: JqQJHCR5uYbaFT3F
    _key: '!journal.pages!JaOBWaX6MgURoLHP.JqQJHCR5uYbaFT3F'
    _stats:
      coreVersion: '12.331'
    name: 3.0.1. Types of Dungeons
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.XOJpjqmBzwE4R4n0]{(Index)
        Environment}</p><p>@Compendium[pf-content.pf-rules.v5dHsJ6gCghRXQnL]{3.0.
        Dungeons}</p><ul><li><strong>@Compendium[pf-content.pf-rules.JaOBWaX6MgURoLHP]{3.0.1.
        Types of
        Dungeons}</strong></li><li>@Compendium[pf-content.pf-rules.jgVKZvuXGPxfGXKQ]{3.0.2.
        Dungeon Terrain}</li></ul><hr /><p>@Source[PZO1110;pages=410]</p><p>The
        four basic dungeon types are defined by their current status. Many
        dungeons are variations on these basic types or combinations of more
        than one of them. Sometimes old dungeons are used again and again by
        different inhabitants for different purposes.</p><h2>Ruined
        Structure</h2><p>Once occupied, this place is now abandoned (completely
        or in part) by its original creator or creators, and other creatures
        have wandered in. Many subterranean creatures look for abandoned
        underground constructions in which to make their lairs. Any traps that
        might exist have probably been set off, but wandering beasts might very
        well be common.</p><h2>Occupied Structure</h2><p>This type of dungeon is
        still in use. Creatures (usually intelligent) live there, although they
        might not be the dungeon’s creators. An occupied structure might be a
        home, a fortress, a temple, an active mine, a prison, or a headquarters.
        This type of dungeon is less likely to have traps or wandering beasts,
        and more likely to have organized guards—both on watch and on patrol.
        Traps or wandering beasts that might be encountered are usually under
        the control of the occupants. Occupied structures have furnishings to
        suit the inhabitants, as well as decorations, supplies, and the ability
        for occupants to move around. The inhabitants might have a communication
        system, and they almost certainly control an exit to the
        outside.</p><p>Some dungeons are partially occupied and partially empty
        or in ruins. In such cases, the occupants are typically not the original
        builders, but instead a group of intelligent creatures that have set up
        their base, lair, or fortification within an abandoned
        dungeon.</p><h2>Safe Storage</h2><p>When people want to protect
        something, they sometimes bury it underground. Whether the item they
        want to protect is a fabulous treasure, a forbidden artifact, or the
        dead body of an important figure, these valuable objects are placed
        within a dungeon and surrounded by barriers, traps, and guardians. The
        safe storage dungeon is the most likely to have traps but the least
        likely to have wandering beasts. This type of dungeon is normally built
        for function rather than appearance, but sometimes it has ornamentation
        in the form of statuary or painted walls. This is particularly true of
        the tombs of important people.</p><p>Sometimes, however, a vault or a
        crypt is constructed in such a way as to house living guardians. The
        problem with this strategy is that something must be done to keep the
        creatures alive between intrusion attempts. Magic is usually the best
        solution to provide food and water for these creatures. Builders of
        vaults or tombs often use undead creatures or constructs, both of which
        have no need for sustenance or rest, to guard their dungeons. Magic
        traps can attack intruders by summoning monsters into the dungeon that
        disappear when their task is done.</p><h2>Natural Cavern
        Complex</h2><p>Underground caves provide homes for all sorts of
        subterranean monsters. Created naturally and connected by labyrinthine
        tunnel systems, these caverns lack any sort of pattern, order, or
        decoration. With no intelligent force behind its construction, this type
        of dungeon is the least likely to have traps or even doors.</p><p>Fungi
        of all sorts thrive in caves, sometimes growing in huge forests of
        mushrooms and puff balls. Subterranean predators prowl these forests,
        looking for weaker creatures feeding upon the fungi. Some varieties of
        fungus give off a phosphorescent glow, providing a natural cavern
        complex with its own limited light source. In other areas, a daylight
        spell or similar magical effect can provide enough light for green
        plants to grow.</p><p>Natural cavern complexes often connect with other
        types of dungeons, the caves having been discovered when the
        manufactured dungeons were delved. A cavern complex can connect two
        otherwise unrelated dungeons, sometimes creating a strange mixed
        environment. A natural cavern complex joined with another dungeon often
        provides a route by which subterranean creatures find their way into a
        manufactured dungeon and populate it.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
