_id: WNrj8ktGtzmJ2idV
_key: '!journal!WNrj8ktGtzmJ2idV'
_stats:
  coreVersion: '12.331'
folder: tfgLGI205b9dxEjx
name: 6.2.2.03. Propulsion and Driving Checks
pages:
  - _id: MdKi4pOZsCcyJ7BI
    _key: '!journal.pages!WNrj8ktGtzmJ2idV.MdKi4pOZsCcyJ7BI'
    _stats:
      coreVersion: '12.331'
    name: 6.2.2.03. Propulsion and Driving Checks
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.4TX2Q1MaWt1B8298]{(Index)
        Items}</p><p>@Compendium[pf-content.pf-rules.F9hzOnUh5c0Ls2p6]{6.2.
        Vehicles}</p><ul><li><p>@Compendium[pf-content.pf-rules.sjmHLtUHjp10PRNg]{6.2.2.
        Full Vehicle
        Rules}</p><ul><li>@Compendium[pf-content.pf-rules.CQgsuMCjnSXQT5tK]{6.2.2.01.
        Drivers}</li><li>@Compendium[pf-content.pf-rules.n20ux7F2Gua3Henu]{6.2.2.02.
        Occupants}</li><li><strong>@Compendium[pf-content.pf-rules.WNrj8ktGtzmJ2idV]{6.2.2.03.
        Propulsion and Driving
        Checks}</strong></li><li>@Compendium[pf-content.pf-rules.bqobhetOALHmOywu]{6.2.2.04.
        Vehicle Size and
        Space}</li><li>@Compendium[pf-content.pf-rules.xPL69LmsvZhMQXkI]{6.2.2.05.
        Vehicle Facing and
        Movement}</li><li>@Compendium[pf-content.pf-rules.wYDD2ws3dIeQaHKV]{6.2.2.06.
        Driving
        Vehicles}</li><li>@Compendium[pf-content.pf-rules.AoRyhHSyrQd2YNsf]{6.2.2.07.
        Optional Rule: Wide
        Turns}</li><li>@Compendium[pf-content.pf-rules.uCzllyIwfv6C8w51]{6.2.2.08.
        Vehicles in
        Combat}</li><li>@Compendium[pf-content.pf-rules.RQx3RwhdQds2znsh]{6.2.2.09.
        Propulsion
        Devies}</li><li>@Compendium[pf-content.pf-rules.UkducnpdR3NCcjdK]{6.2.2.10.
        Driving Devices}</li></ul></li></ul><hr
        /><p>@Source[PZO1118;pages=171]</p><p>Every vehicle has a method of
        propulsion. Creatures pull chariots and wagons. Boats and ships are
        propelled by water currents, wind, muscle, or all three forces.
        Fantastical airships are held aloft by a variety of propulsion sources,
        both magical and mundane. The method of propulsion typically affects the
        speed and maneuverability of a vehicle, but more importantly, a
        vehicle’s propulsion determines the required skill needed to control a
        vehicle. The following are the general methods of vehicle propulsion,
        along with what skills are typically needed to drive such
        vehicles.</p><h2>Alchemical</h2><p>Rarely, an alchemical engine may
        propel a vehicle. Powered by steam or more volatile gases and reagents,
        a vehicle with an alchemical engine requires either a Knowledge (arcana)
        or Craft (alchemy) check to be driven. The base DC to drive an
        alchemical vehicle is 10 higher than normal. Alchemical engines can be
        extremely powerful, with the ability to propel vehicles hundreds of
        times their size. They can also be very fickle when driven by creatures
        uninitiated in the secrets of alchemy.</p><p>When a driver makes a
        driving check to control an alchemically propelled vehicle with a Wisdom
        check or a skill she is not trained in and rolls a natural 1, the
        vehicle’s alchemical engine gains the broken condition. When it gains
        the broken condition, the vehicle’s maximum speed and acceleration are
        both halved, and if the vehicle is currently moving at a rate faster
        than its new maximum speed, it immediately slows to that
        speed.</p><h2>Current</h2><p>From canoes and large ships to winged
        gliders, vehicles propelled by currents typically manipulate an already
        existing power source within or outside of nature— an air current, a
        water current, or more exotic currents, like conduits of magical energy.
        Usually, manipulating a current-propelled vehicle requires a skill like
        Fly, Knowledge (nature), Profession (sailor), Survival, or even
        Acrobatics or Knowledge (arcana), depending on the nature or makeup of
        the vehicle and the current the vehicle is manipulating.</p><h2>Water
        Current</h2><p>Vehicles that only rely on currents of water for their
        propulsion are somewhat limited. These vehicles can only move in the
        direction and at the speed of a current unless they also employ some
        other means of propulsion or manipulation, and thus often have an
        additional form of propulsion, such as muscle in the case of a canoe,
        and wind in the case of a galley. A current-driven ship such as a river
        barge with a crew of two or more creatures requires either a Profession
        (sailor) or Knowledge (nature) check for the driving check, as ships
        require precision, discipline, and knowledge of the natural world.
        Smaller water-current vehicles, like canoes, use the Survival skill as
        the drive skill, as reading the terrain is a very important aspect of
        maintaining control over those types of vehicles.</p><p>If it moves with
        the current, a water-current vehicle’s maximum speed depends on the
        speed of the current (often as high as 120 feet). The acceleration of a
        water-current vehicle is 30 feet.</p><h2>Air Current</h2><p>Air-current
        vehicles are rather diverse. They can be sailing ships, airships, land
        ships, or even gliders. A vehicle propelled by air with a crew of two or
        more creatures requires a Profession (sailor) or Knowledge (nature)
        check as its driving check. Because of their complexity, air-current
        vehicles always have their driving check DCs increased by 10. Smaller
        air-current vehicles, such as gliders and wind sleds, use Acrobatics or
        Fly instead. Much of their control depends on knowledge of flight or
        proper movements of the body to control the vehicle.</p><p>Smaller
        vehicles (size Large or smaller) can move at a speed of 60 feet, can
        move at twice that amount when they are moving with the air current, and
        have an acceleration of 30 feet. Larger vehicles can move at a speed of
        90 feet, or twice that amount when they are moving with the air current,
        and have an acceleration of 30 feet.</p><h2>Weird
        Current</h2><p>Navigating currents of magical energy, burning magma, or
        the murky rivers of the Shadow Plane could use a number of skills, but
        likely use skills similar to those needed to operate water-current and
        air-current vehicles. Weird-current vehicles always have their driving
        check DCs increased by 10, and sometimes by 15 in more exotic locales
        and conditions.</p><p>Weird-current vehicles typically move at the speed
        of water or air currents, depending on their nature, but have been known
        to move twice or even triple those speeds.</p><h2>Magic</h2><p>Magic
        provides some of the most powerful and easy-to-use methods of propelling
        a vehicle, such as an elemental-powered juggernaut or an airship with an
        arcane device at its heart. Often simply identifying the properties of
        the magic item providing propulsion gives a creature the ability to use
        it, but sometimes more complicated magical devices require Spellcraft or
        Use Magic Device to drive properly.</p><h2>Muscle</h2><p>From a chariot
        to a slave ship filled with captive rowers, moving a vehicle powered by
        muscle is all about getting a creature or creatures to push, pull, or
        otherwise propel the vehicle. Based on the type and intelligence of the
        creatures moving the vehicle, checks for driving muscle-propelled
        vehicles can use a diverse number of skills, including, but not limited
        to, Diplomacy, Handle Animal, Intimidate, and Profession
        (driver).</p><p>Muscle-propelled vehicles come in two forms: pulled and
        pushed.</p><h2>Pulled</h2><p>This type of propulsion involves one or
        more creature pulling a vehicle. Unless the creature pulling the vehicle
        is intelligent (Intelligence score of 3 or higher), either Handle Animal
        or Profession (driver) is used for the driving check (driver’s choice).
        Intelligent creatures must be convinced with a Diplomacy check (decrease
        the driving check by 5 if the creature or creatures have the helpful
        attitude), or forced with an Intimidate check. Forcing an intelligent
        creature to pull a vehicle increases the DC by 20.</p><p>A creature can
        pull a number of vehicle squares equal to the number of squares in the
        creature’s space to a top speed equal to twice the creature’s speed. It
        can accelerate its space in vehicle squares up to its speed. For
        instance, a single horse takes up 4 squares, and can pull a 4-square
        cart 100 feet each round with an acceleration of 50
        feet.</p><h2>Pushed</h2><p>Pushed vehicles are the exact opposite of
        pulled vehicles—vehicles that are pushed by muscle, usually using some
        form of device manipulated by crew members. Aquatic vehicles are the
        most likely to be pushed. Lines of rowers use their oars to push the
        vehicle forward, or a pair of cloud giants may churn a propeller at the
        aft end of a dirigible. Driving checks for pushed vehicles tend to be
        Diplomacy, Intimidate, or Handle Animal, depending on the intelligence
        and attitude of the creatures supplying the muscle for the
        propulsion.</p><p>For intelligent creatures, use Diplomacy if the
        creatures providing the propulsion have an attitude of indifferent,
        friendly, or helpful (Pathfinder RPG Core Rulebook 94). Decrease the
        Diplomacy driving check by 5 if the creatures providing the propulsion
        are friendly. Intimidate is used for intelligent creatures with an
        attitude of unfriendly or hostile. Handle Animal is used if the
        creatures providing the propulsion are not intelligent.</p><p>A creature
        that is pushing a vehicle with the proper mechanical help can push
        between 5 times to 20 times its space in vehicle squares.</p><p>The
        maximum speed and acceleration of a muscle-propelled vehicle depends on
        the mechanism used to assist the pushing—see the sample vehicle
        statistics for examples.</p><h2>Mixed Methods of Propulsion</h2><p>Large
        and complicated vehicles, such as large sailing ships, often use
        multiple forms of propulsion. Sometimes multiple methods add
        flexibility, but often they work in concert to create faster movement. A
        vehicle with multiple methods of propulsion often requires a large crew
        to get it going and keep it moving. If a vehicle has two methods of
        propulsion, it uses its fastest speed and acceleration and then adds
        half the speed and acceleration of the secondfastest propulsion. Nothing
        is added for a third form of propulsion, except for the flexibility of
        having a back-up form of propulsion.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
