_id: pJMFHXBSpUdPcgW2
_key: '!journal!pJMFHXBSpUdPcgW2'
_stats:
  coreVersion: '12.331'
folder: CMTuXqZ6Ywxqk9Ar
name: 7.1.3.4.09. Elementals and Genies
pages:
  - _id: ZBhhwbiWqCq89MlB
    _key: '!journal.pages!pJMFHXBSpUdPcgW2.ZBhhwbiWqCq89MlB'
    _stats:
      coreVersion: '12.331'
    name: 7.1.3.4.09. Elementals and Genies
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.dS0UkKOINqI2iTDX]{(Index)
        Magic}</p><p>@Compendium[pf-content.pf-rules.zCeqoMSKdZ8r4EPd]{7.1.
        Mastering
        Magic}</p><ul><li><p>@Compendium[pf-content.pf-rules.Sz0GZaZV3zxb9UTY]{7.1.3.
        Binding
        Outsiders}</p><ul><li><p>@Compendium[pf-content.pf-rules.M5p2I9If5lZHg5iI]{7.1.3.4.
        Outsider
        Categories}</p><ul><li>@Compendium[pf-content.pf-rules.7ajO1mDHRnt1zwri]{7.1.3.4.01.
        Aeon}</li><li>@Compendium[pf-content.pf-rules.CQQmNNUsLaq64ISd]{7.1.3.4.02.
        Agathion}</li><li>@Compendium[pf-content.pf-rules.doRTEmrFcb3Jpd6H]{7.1.3.4.03.
        Angels}</li><li>@Compendium[pf-content.pf-rules.vWKMVgMS2JLVFhSW]{7.1.3.4.04.
        Archons}</li><li>@Compendium[pf-content.pf-rules.ALU8fOwwjVflaV63]{7.1.3.4.05.
        Azata}</li><li>@Compendium[pf-content.pf-rules.Jc9NeIDhX4EJy4NY]{7.1.3.4.06.
        Daemons}</li><li>@Compendium[pf-content.pf-rules.rlk3otEreOuYoI8n]{7.1.3.4.07.
        Demons}</li><li>@Compendium[pf-content.pf-rules.iNa7owekV9rX5NlO]{7.1.3.4.08.
        Devils}</li><li><strong>@Compendium[pf-content.pf-rules.pJMFHXBSpUdPcgW2]{7.1.3.4.09.
        Elementals and
        Genies}</strong></li><li>@Compendium[pf-content.pf-rules.bxArpGfr07tVDIL4]{7.1.3.4.10.
        Inevitables}</li><li>@Compendium[pf-content.pf-rules.rA6iR7Xbug6TMgZk]{7.1.3.4.11.
        Proteans}</li><li>@Compendium[pf-content.pf-rules.J4sDwQBfloYeVUD2]{7.1.3.4.12.
        Qlippoth}</li><li>@Compendium[pf-content.pf-rules.zMmfoLzjryBQtFrg]{7.1.3.4.13.
        Other Outsiders}</li></ul></li></ul></li></ul><hr
        /><p>@Source[PZO1117;pages=108]</p><p>True elementals are simple
        creatures, thriving spirits animating bodies of pure elemental matter.
        They regard their lives as an eternal struggle to best themselves and
        each other. Each elemental type is uniquely suited to adapting to
        conditions on its particular plane, and is arrogant about its powers
        when surrounded by its element... and uniquely frightened and cowed when
        shown a greater power or encased in an element not its own. Most
        elementals do not bargain for favors—they respect only strength. Genies
        are the more human-like denizens of the elemental planes, both in shape
        and mentality. They consider themselves physically and culturally
        superior to true elementals.</p><p>In general, one summons an elemental
        for brute work and combat, and a genie for magical power or ancient
        wisdom, and would thus use summon monster and planar binding for those
        tasks, respectively. However, should a conjurer wish to bind an
        elemental as he would a genie or fiend, the ritual is one of wrestling
        with the elemental’s creativity in reaching its home element. Elementals
        are immune to bleed, paralysis, poison, sleep effects, and stunning.
        They are not subject to critical hits, precision-based attacks like
        sneak attack, or flanking.</p><p>Genies seek and value power, though
        they are more brash and boastful than most. It is said the best way to
        secure the attentions of a genie is to speak its true name, and to offer
        it aid in battles against its political enemies—a wise conjurer
        researches the inner battles of geniekind before summoning a genie, or
        else is prepared to cow the genie with strong magic.</p><h2>Air
        Elemental</h2><p>When summoning an air elemental, inscribe the magic
        circle with diamond powder, and release the elemental only once it has
        acquiesced to the caster’s power. This costs an additional 2,000 gp but
        adds +4 to the Charisma check.</p><h2>Belker</h2><p>These dull-witted
        creatures enjoy expensive incense and exotic green woods that create
        heavy smoke.</p><h2>Crysmal</h2><p>Unlike other elementals, crysmals do
        bargain, but agree to a binder’s demands only if offered a substantial
        amount of crystal, which they use for
        reproduction.</p><h2>Djinni</h2><p>Tomes of knowledge or powerful
        wondrous items tempt djinn and grant the caster a +2 bonus on the
        Charisma check.</p><h2>Earth Elemental</h2><p>Prepare the summoning
        chamber with swirling wind- and air-based spells to prevent the
        elemental from touching the floor. Maintaining this state for 5 rounds
        demonstrates the binder’s superiority over the elemental, and grants a
        +4 bonus on the Charisma check.</p><h2>Efreeti</h2><p>These warlike
        creatures value weapons with enhancement bonuses of at least +2 and
        scrolls of 4th-level or higher spells; these provide a +2 bonus on the
        binder’s Charisma check. Efreet also appreciate attractive humanoid
        slaves, which give a +1 bonus on the binder’s Charisma check for every
        10 slaves offered.</p><h2>Fire Elementals</h2><p>When summoning a fire
        elemental, enclose the casting chamber with stone, remove flammable
        materials from the room, and prepare a magic circle and spells to
        protect against fire. Keeping large blocks of ice in the room saps the
        elemental’s power and cows it into submission more quickly, giving the
        caster a +4 bonus on the Charisma check to trap the
        elemental.</p><h2>Ice Elemental</h2><p>Binders use actual fire, fire
        spells, protections against cold, and sometimes even fire creatures to
        keep ice elementals at bay during summoning. An ice elemental often
        yields to its binder as soon as serious melting occurs.</p><h2>Invisible
        Stalker</h2><p>The tactics used to bind invisible stalkers are similar
        to those used for binding air elementals. Unlike many elementals,
        invisible stalkers bargain for their services, which is why they are
        often summoned by mortal spellcasters.</p><h2>Janni</h2><p>The weakest
        of the genies, the jann are also proud and prone to insult. Gifts of
        rich fabrics, gems, or jewelry worth 1,000 gp or more soften their
        outlook and give the conjurer a +2 bonus on her Charisma
        check.</p><h2>Lightning Elemental</h2><p>Lightning elementals are
        similar to air elementals in that the same bindings work on them, though
        binders would do well to reinforce such bindings and themselves with
        protections against electrical damage.</p><h2>Marid</h2><p>Capricious
        and unpredictable, marids love performance and art—providing them
        artwork worth 1,000 gp or playing a DC 30 Perform (any) piece (whether
        performed by the binder or his ally) piques their interest long enough
        for the conjurer to gain a +2 Charisma bonus.</p><h2>Magma
        Elemental</h2><p>Magma elementals are composite elemental creatures,
        taking some aspects from earth elementals and fire elementals.
        Unsurprisingly, a mix of binding strategies from those two types of
        elementals often keeps these savage things at bay.</p><h2>Mihstu (SR
        19)</h2><p>The strategy for binding and commanding these creatures is
        similar to that for an air elemental. Unlike other elementals, mihstu
        typically bargain with the binder, as many wish to haunt the dark places
        of the Material Plane.</p><h2>Mud Elemental</h2><p>While mud elementals
        vary in consistency, all of them fear becoming too watery or too dry, as
        both conditions have adverse effects on them. Using air spells and
        petrification spells as punishments for noncompliance usually makes
        these elementals more willing to take direction and enter into
        negotiations for appropriate gifts for services
        rendered.</p><h2>Sandman</h2><p>Subtle and craftier than other
        elementals, these creatures nearly always bargain with their binder, but
        are arrogant and headstrong. Even after their task is done, they
        voluntarily remain on the Material Plane to wreak as much havoc as
        possible.</p><h2>Shaitan</h2><p>As the most dense and brash of the
        genies, shaitans like games of chance and physical skill best of all.
        Should the caster offer services in exchange for a throw of the dice or
        a wrestling match—and go through with the offer—he’ll receive a +2 bonus
        on his Charisma check.</p><h2>Thoqqua</h2><p>Natives of the harsh
        landscape where the Planes of Fire and Earth collide, thoqquas are
        dangerous creatures to summon and bind, and typically rage against their
        confines, even when properly bound. Those wishing to bind a thoqqua
        frequently use mephit intermediaries, as those creatures seem to
        understand thoqquas enough to calm them.</p><h2>Water
        Elemental</h2><p>When summoning a water elemental, remove sources of
        water from the room and prepare a bonfire. A ring of fire around the
        magic circle exposes the elemental to its hated enemy the instant it
        appears on the Material Plane, distracting it long enough for the binder
        to seize control. This is an opposed Will check, granting a +1 bonus to
        the caster for each large fire in the room; success grants a +4 bonus on
        the Charisma check.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
