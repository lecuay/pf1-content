_id: yamoxc6efGhmX3cm
_key: '!journal!yamoxc6efGhmX3cm'
_stats:
  coreVersion: '12.331'
folder: YdfdDwTv0nzadnlU
name: 1.1. Common Terms
pages:
  - _id: uyHA3LHkMVfzolnx
    _key: '!journal.pages!yamoxc6efGhmX3cm.uyHA3LHkMVfzolnx'
    _stats:
      coreVersion: '12.331'
    name: 1.1 Common Terms
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.8scSEgBEtJZj0suf]{(Index) Basic
        Rules}</p><hr /><p>@Source[PZO1110;pages=11]</p><p>The Pathfinder RPG
        uses a number of terms, abbreviations, and definitions in presenting the
        rules of the game. The following are among the most common.</p><hr
        /><p><strong>Ability Score</strong>: Each creature has six ability
        scores: Strength, Dexterity, Constitution, Intelligence, Wisdom, and
        Charisma. These scores represent a creature’s most basic attributes. The
        higher the score, the more raw potential and talent your character
        possesses.</p><p><strong>Action</strong>: An action is a discrete
        measurement of time during a round of combat. Using abilities, casting
        spells, and making attacks all require actions to perform. There are a
        number of different kinds of actions, such as a standard action, move
        action, swift action, free action, and full-round action (see Chapter
        8).</p><p><strong>Alignment</strong>: Alignment represents a creature’s
        basic moral and ethical attitude. Alignment has two components: one
        describing whether a creature is lawful, neutral, or chaotic, followed
        by another that describes whether a character is good, neutral, or evil.
        Alignments are usually abbreviated using the first letter of each
        alignment component, such as LN for lawful neutral or CE for chaotic
        evil. Creatures that are neutral in both components are denoted by a
        single “N.”</p><p><strong>Armor Class (AC)</strong>: All creatures in
        the game have an Armor Class. This score represents how hard it is to
        hit a creature in combat. As with other scores, higher is
        better.</p><p><strong>Base Attack Bonus (BAB)</strong>: Each creature
        has a base attack bonus and it represents its skill in combat. As a
        character gains levels or Hit Dice, his base attack bonus improves. When
        a creature’s base attack bonus reaches +6, +11, or +16, he receives an
        additional attack in combat when he takes a full-attack action (which is
        one type of full-round action—see Chapter
        8).</p><p><strong>Bonus</strong>: Bonuses are numerical values that are
        added to checks and statistical scores. Most bonuses have a type, and as
        a general rule, bonuses of the same type are not cumulative (do not
        “stack”)—only the greater bonus granted applies.</p><p><strong>Caster
        Level (CL)</strong>: Caster level represents a creature’s power and
        ability when casting spells. When a creature casts a spell, it often
        contains a number of variables, such as range or damage, that are based
        on the caster’s level.</p><p><strong>Class</strong>: Classes represent
        chosen professions taken by characters and some other creatures. Classes
        give a host of bonuses and allow characters to take actions that they
        otherwise could not, such as casting spells or changing shape. As a
        creature gains levels in a given class, it gains new, more powerful
        abilities. Most PCs gain levels in the core classes or prestige classes,
        since these are the most powerful (see Chapters 3 and 11). Most NPCs
        gain levels in NPC classes, which are less powerful (see Chapter
        14).</p><p><strong>Check</strong>: A check is a d20 roll which may or
        may not be modified by another value. The most common types are attack
        rolls, skill checks, ability checks, and saving
        throws.</p><p><strong>Combat Maneuver</strong>: This is an action taken
        in combat that does not directly cause harm to your opponent, such as
        attempting to trip him, disarm him, or grapple with him (see Chapter
        8).</p><p><strong>Combat Maneuver Bonus (CMB)</strong>: This value
        represents how skilled a creature is at performing a combat maneuver.
        When attempting to perform a combat maneuver, this value is added to the
        character’s d20 roll.</p><p><strong>Combat Maneuver Defense
        (CMD)</strong>: This score represents how hard it is to perform a combat
        maneuver against this creature. A creature’s CMD is used as the
        difficulty class when performing a maneuver against that
        creature.</p><p><strong>Concentration Check</strong>: When a creature is
        casting a spell, but is disrupted during the casting, he must make a
        concentration check or fail to cast the spell (see Chapter
        9).</p><p><strong>Creature</strong>: A creature is an active participant
        in the story or world. This includes PCs, NPCs, and
        monsters.</p><p><strong>Damage Reduction (DR)</strong>: Creatures that
        are resistant to harm typically have damage reduction. This amount is
        subtracted from any damage dealt to them from a physical source. Most
        types of DR can be bypassed by certain types of weapons. This is denoted
        by a “/” followed by the type, such as “10/cold iron.” Some types of DR
        apply to all physical attacks. Such DR is denoted by the “—” symbol. See
        Appendix 1 for more information.</p><p><strong>Difficulty Class
        (DC)</strong>: Whenever a creature attempts to perform an action whose
        success is not guaranteed, he must make some sort of check (usually a
        skill check). The result of that check must meet or exceed the
        Difficulty Class of the action that the creature is attempting to
        perform in order for the action to be
        successful.</p><p><strong>Extraordinary Abilities (Ex)</strong>:
        Extraordinary abilities are unusual abilities that do not rely on magic
        to function.</p><p><strong>Experience Points (XP)</strong>: As a
        character overcomes challenges, defeats monsters, and completes quests,
        he gains experience points. These points accumulate over time, and when
        they reach or surpass a specific value, the character gains a
        level.</p><p><strong>Feat</strong>: A feat is an ability a creature has
        mastered. Feats often allow creatures to circumvent rules or
        restrictions. Creatures receive a number of feats based off their Hit
        Dice, but some classes and other abilities grant bonus
        feats.</p><p><strong>Game Master (GM)</strong>: A Game Master is the
        person who adjudicates the rules and controls all of the elements of the
        story and world that the players explore. A GM’s duty is to provide a
        fair and fun game.</p><p><strong>Hit Dice (HD)</strong>: Hit Dice
        represent a creature’s general level of power and skill. As a creature
        gains levels, it gains additional Hit Dice. Monsters, on the other hand,
        gain racial Hit Dice, which represent the monster’s general prowess and
        ability. Hit Dice are represented by the number the creature possesses
        followed by a type of die, such as “3d8.” This value is used to
        determine a creature’s total hit points. In this example, the creature
        has 3 Hit Dice. When rolling for this creature’s hit points, you would
        roll a d8 three times and add the results together, along with other
        modifiers.</p><p><strong>Hit Points (hp)</strong>: Hit points are an
        abstraction signifying how robust and healthy a creature is at the
        current moment. To determine a creature’s hit points, roll the dice
        indicated by its Hit Dice. A creature gains maximum hit points if its
        first Hit Die roll is for a character class level. Creatures whose first
        Hit Die comes from an NPC class or from his race roll their first Hit
        Die normally. Wounds subtract hit points, while healing (both natural
        and magical) restores hit points. Some abilities and spells grant
        temporary hit points that disappear after a specific duration. When a
        creature’s hit points drop below 0, it becomes unconscious. When a
        creature’s hit points reach a negative total equal to its Constitution
        score, it dies.</p><p><strong>Initiative</strong>: Whenever combat
        begins, all creatures involved in the battle must make an initiative
        check to determine the order in which creatures act during combat. The
        higher the result of the check, the earlier a creature gets to
        act.</p><p><strong>Level</strong>: A character’s level represents his
        overall ability and power. There are three types of levels. Class level
        is the number of levels of a specific class possessed by a character.
        Character level is the sum of all of the levels possessed by a character
        in all of his classes. In addition, spells have a level associated with
        them numbered from 0 to 9. This level indicates the general power of the
        spell. As a spellcaster gains levels, he learns to cast spells of a
        higher level.</p><p><strong>Monster</strong>: Monsters are creatures
        that rely on racial Hit Dice instead of class levels for their powers
        and abilities (although some possess class levels as well). PCs are
        usually not monsters.</p><p><strong>Multiplying</strong>: When you are
        asked to apply more than one multiplier to a roll, the multipliers are
        not multiplied by one another. Instead, you combine them into a single
        multiplier, with each extra multiple adding 1 less than its value to the
        first multiple. For example, if you are asked to apply a ×2 multiplier
        twice, the result would be ×3, not ×4.</p><p><strong>Nonplayer Character
        (NPC)</strong>: These are characters controlled by the
        GM.</p><p><strong>Penalty</strong>: Penalties are numerical values that
        are subtracted from a check or statistical score. Penalties do not have
        a type and most penalties stack with one another.</p><p><strong>Player
        Character (Character, PC)</strong>: These are the characters portrayed
        by the players.</p><p><strong>Round</strong>: Combat is measured in
        rounds. During an individual round, all creatures have a chance to take
        a turn to act, in order of initiative. A round represents 6 seconds in
        the game world.</p><p><strong>Rounding</strong>: Occasionally the rules
        ask you to round a result or value. Unless otherwise stated, always
        round down. For example, if you are asked to take half of 7, the result
        would be 3.</p><p><strong>Saving Throw</strong>: When a creature is the
        subject of a dangerous spell or effect, it often receives a saving throw
        to mitigate the damage or result. Saving throws are passive, meaning
        that a character does not need to take an action to make a saving
        throw—they are made automatically. There are three types of saving
        throws: Fortitude (used to resist poisons, diseases, and other bodily
        ailments), Ref lex (used to avoid effects that target an entire area,
        such as fireball), and Will (used to resist mental attacks and
        spells).</p><p><strong>Skill</strong>: A skill represents a creature’s
        ability to perform an ordinary task, such as climb a wall, sneak down a
        hallway, or spot an intruder. The number of ranks possessed by a
        creature in a given skill represents its proficiency in that skill. As a
        creature gains Hit Dice, it also gains additional skill ranks that can
        be added to its skills.</p><p><strong>Spell</strong>: Spells can perform
        a wide variety of tasks, from harming enemies to bringing the dead back
        to life. Spells specify what they can target, what their effects are,
        and how they can be resisted or negated.</p><p><strong>Spell-Like
        Abilities (Sp)</strong>: Spell-like abilities function just like spells,
        but are granted through a special racial ability or by a specific class
        ability (as opposed to spells, which are gained by spellcasting classes
        as a character gains levels).</p><p><strong>Spell Resistance
        (SR)</strong>: Some creatures are resistant to magic and gain spell
        resistance. When a creature with spell resistance is targeted by a
        spell, the caster of the spell must make a caster level check to see if
        the spell affects the target. The DC of this check is equal to the
        target creature’s SR (some spells do not allow SR
        checks).</p><p><strong>Stacking</strong>: Stacking refers to the act of
        adding together bonuses or penalties that apply to one particular check
        or statistic. Generally speaking, most bonuses of the same type do not
        stack. Instead, only the highest bonus applies. Most penalties do stack,
        meaning that their values are added together. Penalties and bonuses
        generally stack with one another, meaning that the penalties might
        negate or exceed part or all of the bonuses, and vice
        versa.</p><p><strong>Supernatural Abilities (Su)</strong>: Supernatural
        abilities are magical attacks, defenses, and qualities. These abilities
        can be always active or they can require a specific action to utilize.
        The supernatural ability’s description includes information on how it is
        used and its effects.</p><p><strong>Turn</strong>: In a round, a
        creature receives one turn, during which it can perform a wide variety
        of actions. Generally in the course of one turn, a character can perform
        one standard action, one move action, one swift action, and a number of
        free actions. Less-common combinations of actions are permissible as
        well, see Chapter 8 for more details.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
