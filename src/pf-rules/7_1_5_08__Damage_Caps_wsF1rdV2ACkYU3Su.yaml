_id: wsF1rdV2ACkYU3Su
_key: '!journal!wsF1rdV2ACkYU3Su'
_stats:
  coreVersion: '12.331'
folder: CMTuXqZ6Ywxqk9Ar
name: 7.1.5.08. Damage Caps
pages:
  - _id: 4UCbVOJPyoIZICXu
    _key: '!journal.pages!wsF1rdV2ACkYU3Su.4UCbVOJPyoIZICXu'
    _stats:
      coreVersion: '12.331'
    name: 7.1.5.08. Damage Caps
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.dS0UkKOINqI2iTDX]{(Index)
        Magic}</p><p>@Compendium[pf-content.pf-rules.zCeqoMSKdZ8r4EPd]{7.1.
        Mastering
        Magic}</p><ul><li><p>@Compendium[pf-content.pf-rules.xdyGkidQGN7vYL1E]{7.1.5.
        Designing
        Spells}</p><ul><li>@Compendium[pf-content.pf-rules.7Lc81tg2g1Q6TMPo]{7.1.5.01.
        The Golden
        Rule}</li><li>@Compendium[pf-content.pf-rules.DZBAa9axA5Gv4el0]{7.1.5.02.
        Spell
        Terminology}</li><li>@Compendium[pf-content.pf-rules.dbb5pwAitIa3cqqj]{7.1.5.03.
        Intended
        Level}</li><li>@Compendium[pf-content.pf-rules.1DkXvcE3KMmBww9Z]{7.1.5.04.
        Function}</li><li>@Compendium[pf-content.pf-rules.Xu1jyIRMERPFh8X6]{7.1.5.05.
        Spell
        Research}</li><li>@Compendium[pf-content.pf-rules.fF9HpImlhp8oNX5Q]{7.1.5.06.
        Spell
        Damage}</li><li>@Compendium[pf-content.pf-rules.zrMFTIHfzbQTTwZ1]{7.1.5.07.
        Target}</li><li><strong>@Compendium[pf-content.pf-rules.wsF1rdV2ACkYU3Su]{7.1.5.08.
        Damage
        Caps}</strong></li><li>@Compendium[pf-content.pf-rules.9TyMr3XedNUcWVzB]{7.1.5.09.
        Range}</li><li>@Compendium[pf-content.pf-rules.KlR2PDab6uknvopJ]{7.1.5.10.
        Duration}</li><li>@Compendium[pf-content.pf-rules.x0E7IZ5imhQ5srrf]{7.1.5.11.
        Saving
        Throw}</li><li>@Compendium[pf-content.pf-rules.RRG1A0rBL44Qeskg]{7.1.5.12.
        Spell
        Resistance}</li><li>@Compendium[pf-content.pf-rules.kvl8JOILza5HEj7N]{7.1.5.13.
        Casting
        Time}</li><li>@Compendium[pf-content.pf-rules.wb9tYgBlWX8peqFl]{7.1.5.14.
        Components}</li><li>@Compendium[pf-content.pf-rules.PCvwd6PaQH7mD7o6]{7.1.5.15.
        School}</li><li>@Compendium[pf-content.pf-rules.dmUzb6b2BspbBdDp]{7.1.5.16.
        Bonus
        Type}</li><li>@Compendium[pf-content.pf-rules.gh6sxSeK1RNLLFoN]{7.1.5.17.
        Description}</li><li>@Compendium[pf-content.pf-rules.q6Xva2npOy7fVL42]{7.1.5.18.
        Hierarchy of Attack
        Effects}</li><li>@Compendium[pf-content.pf-rules.s6CWKZDo0zQeRXMJ]{7.1.5.19.
        Depletable
        Statistics}</li><li>@Compendium[pf-content.pf-rules.r8zYCFuq59NEtjkz]{7.1.5.20.
        Core is
        King}</li><li>@Compendium[pf-content.pf-rules.aVUL2wrJszAUzNMK]{7.1.5.21.
        Multipurpose
        Spells}</li><li>@Compendium[pf-content.pf-rules.Ak9Ws3Z16GY6QSoH]{7.1.5.22.
        Choosing
        Descriptors}</li><li>@Compendium[pf-content.pf-rules.baSlaM24kwB68nuf]{7.1.5.23.
        Benchmarks}</li></ul></li></ul><hr
        /><p>@Source[PZO1117;pages=129]</p><p>Low-level damage spells are not as
        good as medium- or high-level damage spells—the game is designed so
        lowerlevel spells eventually reach a maximum amount of damage they can
        deal. This is because if low-level spells continued to increase in
        damage without hitting a maximum amount, they’d rival some higher-level
        spells for effectiveness, and the game isn’t as interesting if casters
        are using the same spells at 20th level as they were at 1st.</p><p>The
        maximum damage depends on the level of the spell and whether the spell
        is arcane or divine. This is because arcane magic is deliberately
        designed to be better at dealing damage to balance the fact that divine
        magic is better at healing. A “single target” spell only damages one
        creature (like shocking grasp), or divides its damage among several
        creatures (like burning hands or magic missile). A “multiple target”
        spell applies its full damage to several creatures (like
        fireball).</p><p>For example, a 1st-level single-target wizard spell
        like shocking grasp can deal a maximum of 5 dice of damage (specifically
        5d6). Magic missile can be used against a single target, or the caster
        can split up the missiles to affect multiple creatures, dividing the
        single-target damage among them. Burning hands initially looks like it
        doesn’t obey the damage cap table because it deals multiple dice of
        damage against multiple creatures, but this is offset by the fact that
        it only deals d4s instead of d6s, and it has an extremely close and
        limited area of effect.</p><p>When looking at the Maximum Damage tables,
        also keep in mind that arcane spells usually use d6s for damage and
        divine spells usually use d8s, and these tables assume d6s; when looking
        at the damage caps for divine spells, count each d8 as 2d6. Thus,
        searing light is a 3rd-level single-target cleric spell that deals up to
        5d8 points of damage; treating each d8 as 2d6, that counts as 10d6,
        which is on target for a 3rd-level cleric spell. (Note that the 1d6 per
        level and maximum 10d6 points of damage against undead are still correct
        for a spell of this level, and the slightly higher damage against
        light-vulnerable undead is offset by the reduced damage against
        constructs).</p><p>Tip: If your spell does more damage than the amount
        defined on the table, you should reduce the damage or increase the
        spell’s level.</p><p>Tip: If your spell does less damage than the amount
        defined on the table, you should increase the damage or add another
        effect to the spell. An example of this is sound burst, which only deals
        1d8 points of damage (this amount never increases), but can stun
        creatures in the area.</p><h2>Table 2-5: Maximum Damage for Arcane
        Spells</h2><table><thead><tr><td>Spell Level</td><td>Max Damage (Single
        Target)</td><td>Max Damage (Multiple
        Targets)</td></tr></thead><tbody><tr><td>1st</td><td>5
        dice</td><td>-</td></tr><tr><td>2nd</td><td>10 dice</td><td>5
        dice</td></tr><tr><td>3rd</td><td>10 dice</td><td>10
        dice</td></tr><tr><td>4th</td><td>15 dice</td><td>10
        dice</td></tr><tr><td>5th</td><td>15 dice</td><td>15
        dice</td></tr><tr><td>6th</td><td>20 dice</td><td>15
        dice</td></tr><tr><td>7th</td><td>20 dice</td><td>20
        dice</td></tr><tr><td>8th</td><td>25 dice</td><td>20
        dice</td></tr><tr><td>9th</td><td>25 dice</td><td>25
        dice</td></tr></tbody></table><h2>Table 2-6: Maximum Damage for Divine
        Spells</h2><table><thead><tr><td>Spell Level</td><td>Max Damage (Single
        Target)</td><td>Max Damage (Multiple
        Targets)</td></tr></thead><tbody><tr><td>1st</td><td>1
        die</td><td>-</td></tr><tr><td>2nd</td><td>5 dice</td><td>1
        die</td></tr><tr><td>3rd</td><td>10 dice</td><td>5
        dice</td></tr><tr><td>4th</td><td>10 dice</td><td>10
        dice</td></tr><tr><td>5th</td><td>15 dice</td><td>10
        dice</td></tr><tr><td>6th</td><td>15 dice</td><td>15
        dice</td></tr><tr><td>7th</td><td>20 dice</td><td>15
        dice</td></tr><tr><td>8th</td><td>20 dice</td><td>20
        dice</td></tr><tr><td>9th</td><td>25 dice</td><td>20
        dice</td></tr></tbody></table>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
