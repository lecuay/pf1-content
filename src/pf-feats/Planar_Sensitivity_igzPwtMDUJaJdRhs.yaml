_id: igzPwtMDUJaJdRhs
_key: '!items!igzPwtMDUJaJdRhs'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/skills/blue_06.jpg
name: Planar Sensitivity
system:
  description:
    value: >-
      <p><em>You can sense and manipulate connections between the
      planes.</em></p><p><strong>Prerequisites</strong>: Knowledge (planes) 3
      ranks.</p><p><strong>Benefits</strong>: For every summon nature's ally
      extract you know, you learn the equivalent summon monster spell as an
      extract. If you later learn other summon nature's ally extracts, you
      automatically learn the equivalent summon monster spell as an
      extract.</p><p><strong>Special</strong>: Sentient outsiders who have one
      or more ranks in Knowledge (planes) and who lack the native subtype are
      treated as having this feat.</p><h2>Gatefinder (Perception)</h2><p>You can
      notice soulgates compatible with your
      alignment.</p><p><strong>Check</strong>: You are familiar with soulgates
      and might notice those with an alignment matching your own. To notice a
      soulgate that you&rsquo;re not already familiar with, you must succeed at
      a DC 20 Perception check&mdash;this DC could be higher if the portal is
      obscured or damaged, or at the GM&rsquo;s discretion. If the soulgate
      shares your exact alignment, you gain a +5 bonus on your check to notice
      the portal, feeling an instinctual draw toward it. If your alignment and
      the portal&rsquo;s share none of the same components, you take a &ndash;5
      penalty on your check to notice the portal. If your alignment is neutral
      with no other alignment components, you take no penalties on checks to
      find soulgates.</p><p>For example, a lawful good character would gain a +5
      bonus on her Perception checks to find lawful good soulgates, but would
      take a &ndash;5 penalty on checks to find chaotic evil, chaotic neutral,
      and neutral evil soulgates, as she has no chaotic, neutral, or evil
      components to her alignment. A neutral character, however, gains a +5
      bonus on checks to find neutral gates, but gains no bonus on checks to
      find chaotic neutral, lawful neutral, neutral good, and neutral evil
      soulgates. Additionally, she takes no penalty on checks to notice chaotic
      good, chaotic evil, lawful good, or lawful evil soulgates.</p><p>Once you
      are aware of a soulgate, you never need to succeed at a Perception check
      to find that particular gate again, as long as some remarkable event
      doesn&rsquo;t change its appearance or location.</p><h2>Gatekeeper
      (Knowledge [Planes])</h2><p>You can attempt to open soulgates compatible
      with your alignment.</p><p><strong>Check</strong>: Once you are aware of a
      soulgate, you can use your knowledge of the planes and your place as a
      child of the multiverse to attempt to open the portal. This attempt takes
      1 minute to perform. The DC of the Knowledge (planes) check to open a
      soulgate is 30. If your alignment perfectly matches that of the soulgate,
      you gain a +5 bonus on this check. You can also attempt to open a soulgate
      with an alignment that is within one step of your own. However, unless you
      are neutral, you cannot open a soulgate with an alignment two or more
      steps away from your own.</p><p>For example, a lawful evil character would
      gain a +5 bonus on her attempt to open a soulgate connecting the Material
      Plane to Hell (lawful evil) or other planes of lawful evil alignment. She
      can also attempt to open soulgates to Abaddon (neutral evil), Axis (lawful
      neutral), and other planes that share those planes&rsquo; alignments,
      because their alignments are one step away from lawful evil. She can never
      open a soulgate to the Abyss (chaotic evil), the Boneyard (neutral),
      Elysium (chaotic good), Heaven (lawful good), the Maelstrom (chaotic
      neutral), Nirvana (neutral good), or any other plane with those
      alignments.</p><p>If your alignment is neutral with no other alignment
      components, you can open soulgates with an alignment two steps away from
      your own, but you take a &ndash;10 penalty on attempts to do
      so.</p><p>Once opened, you can keep a soulgate open for a number of rounds
      equal to your Charisma modifier. An open soulgate closes at the end of
      that time, or you can close it earlier as a standard action.</p>
  tags:
    - General
type: feat
