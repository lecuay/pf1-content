_id: lqFhyqE7frSbHfEA
_key: '!items!lqFhyqE7frSbHfEA'
_stats:
  coreVersion: '12.331'
folder: 1XqRrec5Ag0n0yM2
img: systems/pf1/icons/races/gnome.png
name: Magical Linguist
system:
  actions:
    - _id: OPVbbPbxDfsCuPaL
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      area: 1 sq ft
      description: >-
        <p>Gnomes with Charisma scores of 11 or higher also gain the following
        spell-like abilities:
        1/day-<em>@UUID[Compendium.pf1.spells.Item.n8k4pdmjjg8xuv7l]{arcane
        mark}</em>,
        <em>@UUID[Compendium.pf1.spells.Item.jcs7ba5depdyitey]{comprehend
        languages}</em>,
        <em>@UUID[Compendium.pf1.spells.Item.msaqovkgfxi6q8s8]{message}</em>,
        <em>@UUID[Compendium.pf1.spells.Item.j0bc0iii8zp1jiwr]{read magic}</em>.
        The caster level for these effects is equal to the gnome's level.</p>
      duration:
        units: perm
      name: Arcane Mark
      notes:
        effect:
          - >-
            This spell allows you to inscribe your personal rune or mark, which
            can consist of no more than six characters. The writing can be
            visible or invisible. An arcane mark spell enables you to etch the
            rune upon any substance without harm to the material upon which it
            is placed. If an invisible mark is made, a detect magic spell causes
            it to glow and be visible, though not necessarily understandable. 
            See invisibility, true seeing, a gem of seeing, or a robe of eyes
            likewise allows the user to see an invisible arcane mark. A read
            magic spell reveals the words, if any. The mark cannot be dispelled,
            but it can be removed by the caster or by an erase spell.  If an
            arcane mark is placed on a living being, the effect gradually fades
            in about a month.  Arcane mark must be cast on an object prior to
            casting instant summons on the same object (see that spell
            description for details).
      range:
        units: touch
      uses:
        self:
          maxFormula: '1'
          per: day
          value: 1
    - _id: QnsZ5RaEhNsUa42V
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      description: >-
        <p>Gnomes with Charisma scores of 11 or higher also gain the following
        spell-like abilities:
        1/day-<em>@UUID[Compendium.pf1.spells.Item.n8k4pdmjjg8xuv7l]{arcane
        mark}</em>,
        <em>@UUID[Compendium.pf1.spells.Item.jcs7ba5depdyitey]{comprehend
        languages}</em>,
        <em>@UUID[Compendium.pf1.spells.Item.msaqovkgfxi6q8s8]{message}</em>,
        <em>@UUID[Compendium.pf1.spells.Item.j0bc0iii8zp1jiwr]{read magic}</em>.
        The caster level for these effects is equal to the gnome's level.</p>
      duration:
        units: minute
        value: '@attributes.hd.total * 10'
      name: Comprehend Languages
      notes:
        effect:
          - >-
            You can understand the spoken words of creatures or read otherwise
            incomprehensible written messages. The ability to read does not
            necessarily impart insight into the material, merely its literal
            meaning. The spell enables you to understand or read an unknown
            language, not speak or write it.  Written material can be read at
            the rate of one page (250 words) per minute. Magical writing cannot
            be read, though the spell reveals that it is magical. This spell can
            be foiled by certain warding magic (such as the secret page and
            illusory script spells). It does not decipher codes or reveal
            messages concealed in otherwise normal text.  Comprehend languages
            can be made permanent with a permanency spell.
      range:
        units: personal
      uses:
        self:
          maxFormula: '1'
          per: day
          value: 1
    - _id: nRXmaxzDI1luHXMr
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      description: >-
        <p>Gnomes with Charisma scores of 11 or higher also gain the following
        spell-like abilities:
        1/day-<em>@UUID[Compendium.pf1.spells.Item.n8k4pdmjjg8xuv7l]{arcane
        mark}</em>,
        <em>@UUID[Compendium.pf1.spells.Item.jcs7ba5depdyitey]{comprehend
        languages}</em>,
        <em>@UUID[Compendium.pf1.spells.Item.msaqovkgfxi6q8s8]{message}</em>,
        <em>@UUID[Compendium.pf1.spells.Item.j0bc0iii8zp1jiwr]{read magic}</em>.
        The caster level for these effects is equal to the gnome's level.</p>
      duration:
        units: minute
        value: '@attributes.hd.total * 10'
      name: Message
      notes:
        effect:
          - >-
            You can whisper messages and receive whispered replies.  Those
            nearby can hear these messages with a DC 25 Perception check. You
            point your finger at each creature you want to receive the message.
            When you whisper, the whispered message is audible to all targeted
            creatures within range.  Magical silence, 1 foot of stone, 1 inch of
            common metal (or a thin sheet of lead), or 3 feet of wood or dirt
            blocks the spell.  The message does not have to travel in a straight
            line. It can circumvent a barrier if there is an open path between
            you and the subject, and the path's entire length lies within the
            spell's range. The creatures that receive the message can whisper a
            reply that you hear. The spell transmits sound, not meaning; it
            doesn't transcend language barriers. To speak a message, you must
            mouth the words and whisper.
      range:
        units: ft
        value: '@attributes.hd.total * 10 + 100'
      target:
        value: up to [[@attributes.hd.total ]] creatures
      uses:
        self:
          maxFormula: '1'
          per: day
          value: 1
    - _id: bNnY5AO2AwOzFqjE
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      description: >-
        <p>Gnomes with Charisma scores of 11 or higher also gain the following
        spell-like abilities:
        1/day-<em>@UUID[Compendium.pf1.spells.Item.n8k4pdmjjg8xuv7l]{arcane
        mark}</em>,
        <em>@UUID[Compendium.pf1.spells.Item.jcs7ba5depdyitey]{comprehend
        languages}</em>,
        <em>@UUID[Compendium.pf1.spells.Item.msaqovkgfxi6q8s8]{message}</em>,
        <em>@UUID[Compendium.pf1.spells.Item.j0bc0iii8zp1jiwr]{read magic}</em>.
        The caster level for these effects is equal to the gnome's level.</p>
      duration:
        units: minute
        value: '@attributes.hd.total * 10'
      name: Read Magic
      notes:
        effect:
          - >-
            You can decipher magical inscriptions on objects-books, scrolls,
            weapons, and the like-that would otherwise be unintelligible. This
            deciphering does not normally invoke the magic contained in the
            writing, although it may do so in the case of a cursed or trapped
            scroll. Furthermore, once the spell is cast and you have read the
            magical inscription, you are thereafter able to read that particular
            writing without recourse to the use of read magic. You can read at
            the rate of one page (250 words) per minute. The spell allows you to
            identify a Glyph of Warding with a DC 13 Spellcraft check, a greater
            Glyph of Warding with a DC 16 Spellcraft check, or any symbol spell
            with a Spellcraft check (DC 10 + spell level).
      range:
        units: personal
      uses:
        self:
          maxFormula: '1'
          per: day
          value: 1
  contextNotes:
    - target: spellEffect
      text: >-
        +[[1]] to the DC of spells with the language-dependent descriptor or
        those that create glyphs, symbols, or other magical writings.
    - target: allSavingThrows
      text: >-
        +[[2]] Racial bonus against spells with the language-dependent
        descriptor or those that create glyphs, symbols, or other magical
        writings.
  description:
    value: >-
      <p><strong>Race</strong>: Gnome<br /><strong>Replaced Trait(s)</strong>:
      Gnome Magic, Illusion Resistance</p><hr /><p>Gnomes study languages in
      both their mundane and supernatural manifestations. Gnomes with this
      racial trait add +1 to the DC of spells they cast with the
      language-dependent descriptor or those that create glyphs, symbols, or
      other magical writings. They gain a +2 racial bonus on saving throws
      against such spells. Gnomes with Charisma scores of 11 or higher also gain
      the following spell-like abilities:
      1/day-<em>@UUID[Compendium.pf1.spells.Item.n8k4pdmjjg8xuv7l]{arcane
      mark}</em>,
      <em>@UUID[Compendium.pf1.spells.Item.jcs7ba5depdyitey]{comprehend
      languages}</em>,
      <em>@UUID[Compendium.pf1.spells.Item.msaqovkgfxi6q8s8]{message}</em>,
      <em>@UUID[Compendium.pf1.spells.Item.j0bc0iii8zp1jiwr]{read magic}</em>.
      The caster level for these effects is equal to the gnome's level. This
      racial trait replaces gnome magic and illusion resistance.</p>
  showInQuickbar: true
  sources:
    - id: PZO1121
      pages: '32'
    - id: PZO1115
      pages: '15'
  subType: racial
  tags:
    - Gnome
  uses:
    maxFormula: '4'
    per: day
    value: 4
type: feat
