_id: 9p6QP7WVvRiQ7bgz
_key: '!actors!9p6QP7WVvRiQ7bgz'
_stats:
  coreVersion: '12.331'
img: icons/svg/mystery-man.svg
items:
  - _id: cV7yHt8i5YCV0ZTd
    _key: '!actors.items!9p6QP7WVvRiQ7bgz.cV7yHt8i5YCV0ZTd'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_40.jpg
    name: Outsider (Azata, Chaotic, Extraplanar, Good)
    system:
      bab: high
      classSkills:
        blf: true
        crf: true
        kpl: true
        per: true
        sen: true
        ste: true
      description:
        value: >-
          <p>An outsider is at least partially composed of the essence (but not
          necessarily the material) of some plane other than the Material Plane.
          Some creatures start out as some other type and become outsiders when
          they attain a higher (or lower) state of spiritual
          existence.</p><h2>Features</h2><p>An outsider has the following
          features.</p><ul><li>d10 Hit Dice.</li><li>Base attack bonus equal to
          total Hit Dice (fast progression).</li><li>Two good saving throws,
          usually&nbsp;<a
          href="https://www.d20pfsrd.com/gamemastering/combat#TOC-Reflex"
          rel="nofollow">Reflex</a>&nbsp;and Will.</li><li>Skill points equal to
          6 + Int modifier (minimum 1) per Hit Die. The following are class
          skills for outsiders: <a href="https://www.d20pfsrd.com/skills/bluff"
          rel="nofollow">Bluff</a>, <a
          href="https://www.d20pfsrd.com/skills/craft" rel="nofollow">Craft</a>,
          <a href="https://www.d20pfsrd.com/skills/knowledge"
          rel="nofollow">Knowledge</a>&nbsp;(planes), <a
          href="https://www.d20pfsrd.com/skills/perception"
          rel="nofollow">Perception</a>, <a
          href="https://www.d20pfsrd.com/skills/sense-motive"
          rel="nofollow">Sense Motive</a>, and <a
          href="https://www.d20pfsrd.com/skills/stealth"
          rel="nofollow">Stealth</a>. Due to their varied nature, outsiders also
          receive 4 additional class skills determined by the creature’s
          theme.</li></ul><h2>Traits</h2><p>An outsider possesses the following
          traits (unless otherwise noted in a creature’s
          entry).</p><ul><li>Darkvision 60 feet.</li><li>Unlike most living
          creatures, an outsider does not have a dual nature—its soul and body
          form one unit. When an outsider is slain, no soul is set loose. Spells
          that restore souls to their bodies, such as&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/r/raise-dead"
          rel="nofollow">raise dead</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/r/reincarnate"
          rel="nofollow">reincarnate</a>,&nbsp;and&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/r/resurrection"
          rel="nofollow">resurrection</a>,&nbsp;don’t work on an outsider. It
          takes a different magical effect, such as&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/l/limited-wish"
          rel="nofollow">limited wish</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/w/wish"
          rel="nofollow">wish</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/m/miracle"
          rel="nofollow">miracle</a>,&nbsp;or&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/t/true-resurrection"
          rel="nofollow">true resurrection</a>&nbsp;to restore it to life. An
          outsider with the native subtype can be raised, reincarnated, or
          resurrected just as other living creatures can be.</li><li>Proficient
          with all simple and martial weapons and any weapons mentioned in its
          entry.</li><li>Proficient with whatever type of armor (light, medium,
          or heavy) it is described as wearing, as well as all lighter types.
          Outsiders not indicated as wearing armor are not proficient with
          armor. Outsiders are proficient with shields if they are proficient
          with any form of armor.</li><li>Outsiders breathe, but do not need to
          eat or sleep (although they can do so if they wish). Native outsiders
          breathe, eat, and sleep.</li></ul>
      hd: 10
      hp: 26
      level: 3
      savingThrows:
        ref:
          value: high
        will:
          value: high
      skillsPerLevel: 6
      tag: outsider
    type: class
  - _id: fFfRYy51Wt4rogld
    _key: '!actors.items!9p6QP7WVvRiQ7bgz.fFfRYy51Wt4rogld'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_40.jpg
    name: 'Race: Outsider'
    system:
      creatureSubtypes:
        - azata
        - chaotic
        - extraplanar
        - good
      creatureTypes:
        - outsider
      description:
        value: >-
          sbc | As the statblock did not include a race, a custom one was
          generated.
    type: race
  - _id: UDi1Hz71GAwYZfmW
    _key: '!actors.items!9p6QP7WVvRiQ7bgz.UDi1Hz71GAwYZfmW'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/shadow_12.jpg
    name: 'Sense: Darkvision 60ft.'
    system:
      abilityType: ex
      description:
        value: >-
          Darkvision is the extraordinary ability to see with no light source at
          all, out to a range specified for the creature. Darkvision is
          black-and-white only (colors cannot be discerned). It does not allow
          characters to see anything that they could not see otherwise—invisible
          objects are still invisible, and illusions are still visible as what
          they seem to be. Likewise, darkvision subjects a creature to gaze
          attacks normally. The presence of light does not spoil darkvision.
      subType: racial
    type: feat
  - _id: zAMOeseHKAgDckeL
    _key: '!actors.items!9p6QP7WVvRiQ7bgz.zAMOeseHKAgDckeL'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/shadow_11.jpg
    name: 'Sense: Low-light Vision'
    system:
      abilityType: ex
      description:
        value: >-
          A creature with low-light vision can see twice as far as a human in
          starlight, moonlight, torchlight, and similar conditions of dim light.
          It retains the ability to distinguish color and detail under these
          conditions.
      subType: racial
    type: feat
  - _id: lxDJhpqwtR3J4UXx
    _key: '!actors.items!9p6QP7WVvRiQ7bgz.lxDJhpqwtR3J4UXx'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/feats/agile-maneuvers.jpg
    name: Agile Maneuvers
    system:
      changes:
        - _id: t7xzgayi
          formula: >-
            (@abilities.dex.mod > @abilities.str.mod ? @abilities.dex.mod -
            @abilities.str.mod : 0)
          target: cmb
          type: untyped
      description:
        value: |2-

                          <em>You've learned to use your quickness in place of brute force when performing combat maneuvers.</em></p><p>
                          <p><strong>Benefits</strong></p><p>You add your Dexterity bonus to your base attack bonus and size bonus when determining your Combat Maneuver Bonus (see Combat) instead of your Strength bonus.</p>
                      <br /><p><strong>Normal</strong></p><p>You add your Strength bonus to your base attack bonus and size bonus when determining your Combat Maneuver Bonus.</p>
      tags:
        - Combat
    type: feat
  - _id: G0nX5YbGVylylm6C
    _key: '!actors.items!9p6QP7WVvRiQ7bgz.G0nX5YbGVylylm6C'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: "Special Quality: Traveler's Friend"
    system:
      description:
        value: >-
          sbc | Placeholder for Special Qualities, which in most statblocks are
          listed under SQ in the statistics block, but described in the Special
          Abilities. Remove duplicates as needed!
      subType: misc
    type: feat
  - _id: Jo79f3DVnWoCLOaU
    _key: '!actors.items!9p6QP7WVvRiQ7bgz.Jo79f3DVnWoCLOaU'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/items/inventory/monster-forearm.jpg
    name: Slam
    system:
      actions:
        - _id: dx6do6viv6ag2u83
          ability:
            attack: _default
            damage: str
          actionType: mwak
          activation:
            type: attack
            unchained:
              type: attack
          attackBonus: '0'
          attackName: Slam
          damage:
            parts:
              - formula: 1d2+0
                types:
                  - bludgeoning
          duration:
            units: inst
          img: systems/pf1/icons/items/inventory/monster-forearm.jpg
          name: Attack
          notes:
            footer:
              - slam +2 (1d2-3)
          range:
            units: melee
          tag: slam
      subType: natural
    type: attack
  - _id: LfPp4S4j2wjW5h9u
    _key: '!actors.items!9p6QP7WVvRiQ7bgz.LfPp4S4j2wjW5h9u'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Environment: Any (elysium)'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: 85WeiJhnGdhN4mez
    _key: '!actors.items!9p6QP7WVvRiQ7bgz.85WeiJhnGdhN4mez'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Organization: Solitary, Band (2-5), Or Company (6-24)'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: vFRD49J7hIMNWHZL
    _key: '!actors.items!9p6QP7WVvRiQ7bgz.vFRD49J7hIMNWHZL'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Treasure: None'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: ZebRkanzohAjfTOl
    _key: '!actors.items!9p6QP7WVvRiQ7bgz.ZebRkanzohAjfTOl'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Starlight Blast
    system:
      abilityType: su
      description:
        value: >-
          As a standard action once every 1d4 rounds, a lyrakien can tap into
          the divine power of Elysium, unleashing a blast of holy starlight in a
          5-foot burst. All creatures in this area take 1d4 points of holy
          damage, plus 1 point for each step their alignment deviates from
          chaotic good. For example, a chaotic neutral or neutral good creature
          would take 1d4+1 points of damage, a neutral creature would take 1d4+2
          points of damage, and a lawful evil creature would take 1d4+4 points
          of damage. A DC 12 Reflex save negates this damage. Chaotic good
          creatures are unaffected by this ability. The save DC is
          Constitution-based.
      subType: classFeat
    type: feat
  - _id: PFi7BLKw8UmJIFvD
    _key: '!actors.items!9p6QP7WVvRiQ7bgz.PFi7BLKw8UmJIFvD'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Traveler's Friend
    system:
      abilityType: su
      description:
        value: >-
          The performances and company of a lyrakien ease the burden of travel.
          Once per day, a creature may spend a minute listening to a lyrakien's
          performance-doing so removes the effects of exhaustion and fatigue
          from the listener.
      subType: classFeat
    type: feat
  - _id: xiGyG6PzZvTcMYLF
    _key: '!actors.items!9p6QP7WVvRiQ7bgz.xiGyG6PzZvTcMYLF'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: sbc | Conversion Buff
    system:
      active: true
      changes:
        - _id: iuv84ivn
          formula: '-10'
          target: mhp
          type: untypedPerm
        - _id: bmt9fjtp
          formula: '-8'
          target: skill.fly
          type: untypedPerm
      description:
        value: >-
          <h2>sbc | Conversion Buff</h2><p>This Buff was created by
          <strong>sbc</strong> to compensate for differences between the input
          and the values FoundryVTT calculates automatically.</p><p>Especially
          when the pathfinder system gets upgraded, entries in compendiums get
          updated or foundry changes in some way, this buff may become
          outdated.</p><p>For most mooks the conversion should more or less
          work, but for important NPCs or creatures it is adviced to double
          check the conversion manually.</p>
      hideFromToken: true
      subType: perm
      tag: sbcConversionBuff
    type: buff
name: Lyrakien
prototypeToken:
  texture:
    src: icons/svg/mystery-man.svg
system:
  abilities:
    cha:
      value: 20
    con:
      value: 12
    dex:
      value: 19
    int:
      value: 14
    str:
      value: 5
    wis:
      value: 17
  attributes:
    speed:
      fly:
        base: 80
        maneuverability: perfect
    spells:
      spellbooks:
        primary:
          name: Primary
        secondary:
          name: Secondary
        spelllike:
          name: Spell-likes
        tertiary:
          name: Tertiary
  details:
    alignment: cg
    cr:
      base: 2
    notes:
      value: "\n        <div class=\"statblockContainer\" style=\"margin-top: 15px\"><div style=\"width: 100%; background-color: #ffe9c7; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); columns: 2 150px\">\r\n    <hr style=\"margin-left: 0; margin-top:-2px; width: 100%; height: 4px; background-color: #e0a100; border: 1px solid #000; column-span: all;\" />\r\n    <div style=\"padding: 5px;\">\r\n        <h1>Lyrakien CR 2 </h1>\r\n        \r\n        <!-- BASE -->\r\n        <div style=\"break-inside: avoid;\">\r\n            <strong>Source</strong>: Bestiary 2 pg. 38, Pathfinder #2: The Skinsaw Murders pg. 86<br/>\r\n            <strong>XP 600</strong><br/>\r\n            \r\n            \r\n            \r\n            \r\n            \r\n            CG \r\n            Tiny \r\n            Outsider (Azata, Chaotic, Extraplanar, Good) <br/>\r\n            <strong>Init</strong> 8;\r\n            <strong>Senses</strong> darkvision 60 ft., detect evil, detect magic, low-light vision; Perception +9; \r\n            \r\n        </div>\r\n        <div style=\"break-inside: avoid;\">\r\n            <h3>Defense</h3>\r\n                <strong>AC</strong> 16, <strong>Touch</strong> 16, <strong>Flat-Footed</strong> 12 (+4 Dex, +2 size)<br/>\r\n            <strong>Hit Points</strong> 19 (3 HD; 3d10+3)<br/>\r\n            <strong>Fort</strong> 2, <strong>Ref</strong> 7, <strong>Will</strong> 6\r\n            \r\n            <strong>DR</strong> 5/evil\r\n            <strong>Immune</strong> electricity, petrification; \r\n            <strong>Resist</strong> cold 10, fire 10; \r\n            \r\n            \r\n            \r\n        </div>\r\n        <div>\r\n            <div style=\"break-inside: avoid;\">\r\n                <h3>Offense</h3>\r\n                        <strong>Speed</strong> 80 ft. (perfect)<br/>\r\n                <strong>Melee</strong> slam +2 (1d2-3)<br/>\r\n                \r\n                \r\n                \r\n                \r\n            </div>\r\n        </div>\r\n        <!-- SPELLCASTING -->\r\n                \r\n        <!-- TACTICS -->\r\n                \r\n                \r\n                \r\n                \r\n        <!-- STATISTICS -->\r\n        <div style=\"break-inside: avoid;\">\r\n            <h3>Statistics</h3>\r\n                <strong>Str</strong> 5, <strong>Dex</strong> 19, <strong>Con</strong> 12, <strong>Int</strong> 14, <strong>Wis</strong> 17, <strong>Cha</strong> 20<br/>\r\n            <strong>Base Atk</strong> +3; <strong>CMB</strong> +5; <strong>CMD</strong> 12<br/>\r\n            <strong>Feats</strong> Agile Maneuvers, Improved Initiative<br/>\r\n            <strong>Skills</strong> Acrobatics +10, Bluff +11, Diplomacy +11, Fly +16, Knowledge (any one) +8, Perception +9, Perform (any one) +11, Spellcraft +5, Stealth +18<br/>\r\n            <strong>Languages</strong> Celestial, Draconic, Infernal, truespeech<br/>\r\n            \r\n            \r\n            \r\n            \r\n        </div>\r\n        <!-- ECOLOGY -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Ecology</h3>                <strong>Environment</strong>: any (Elysium)<br/>\r\n                <strong>Organization</strong>: solitary, band (2-5), or company (6-24)<br/>\r\n                <strong>Treasure</strong>: none<br/>\r\n            </div>\r\n        <!-- SPECIAL ABILITIES -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Special Abilities</h3>                <div style='white-space: pre-wrap;'>Starlight Blast (su): As a standard action once every 1d4 rounds, a lyrakien can tap into the divine power of Elysium, unleashing a blast of holy starlight in a 5-foot burst. All creatures in this area take 1d4 points of holy damage, plus 1 point for each step their alignment deviates from chaotic good. For example, a chaotic neutral or neutral good creature would take 1d4+1 points of damage, a neutral creature would take 1d4+2 points of damage, and a lawful evil creature would take 1d4+4 points of damage. A DC 12 Reflex save negates this damage. Chaotic good creatures are unaffected by this ability. The save DC is Constitution-based.\n\nTraveler&#x27;s Friend (su): The performances and company of a lyrakien ease the burden of travel. Once per day, a creature may spend a minute listening to a lyrakien&#x27;s performance-doing so removes the effects of exhaustion and fatigue from the listener.</div>\r\n            </div>\r\n        \r\n        <!-- DESCRIPTION -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Description</h3>        <div style=\"white-space: pre-wrap\">Lyrakien are divine musicians and messengers, mainly in the employ of deities of travel and natural wonders. They love to explore and visit beautiful places, especially locations with excellent views of rainbows, moonlight, and the stars. Whimsical and joyous, they love contests of song, dance, and knowledge, and keep journeys happy by distracting their companions from weary feet and stale food. Mortals who please them with excellent tales and new songs may be rewarded with elaborate maps, forgotten shortcuts, or rambling directions to hidden locations that hold lost magic.\n\nLyrakien are light-hearted creatures, but they are very protective of breathtaking natural locations. Often called &#x27;glistenwings&#x27; by gnomes and halflings, lyrakien are frequently mistaken for fey-while they are generally friendly with true fey, their origin is the plane of Elysium. Like other azatas, they grow restless if they stay in one place too long. A chaotic good 7th-level spellcaster can gain a lyrakien as a familiar if she has the Improved Familiar feat.\n</div>\r\n                    \r\n    </div>\r\n    <hr style=\"margin-left: 0; margin-bottom: -3px; width: 100%; height: 4px; background-color: #e0a100; border: 1px solid #000; column-span: all;\" />\r\n</div></div>\n    \n        <br>\n        <hr>\n        <div class=\"rawInputContainer\" style=\"margin-top: 15px;\">\n            <h2>RAW INPUT</h2>\n            <hr>\n            <pre style=\"white-space: pre-wrap; font-size: 10px;\">Lyrakien CR 2\nSource Bestiary 2 pg. 38, Pathfinder #2: The Skinsaw Murders pg. 86\nXP 600\nCG Tiny outsider (azata, chaotic, extraplanar, good)\nInit +8; Senses darkvision 60 ft., detect evil, detect magic, low-light vision; Perception +9\nDefense\nAC 16, touch 16, flat-footed 12 (+4 Dex, +2 size)\nhp 19 (3d10+3)\nFort +2, Ref +7, Will +6\nDR 5/evil; Immune electricity, petrification; Resist cold 10, fire 10\nOffense\nSpeed 30 ft., fly 80 ft. (perfect)\nMelee slam +2 (1d2-3)\nSpace 2-1/2 ft., Reach 0 ft.\nSpecial Attacks starlight blast\nSpell-Like Abilities (CL 3rd; concentration +8)\nConstant-detect evil, detect magic, freedom of movement\nAt will-dancing lights, daze (DC 15), summon instrument, ventriloquism (DC 16)\n1/day-cure light wounds, lesser confusion (DC 16), silent image (DC 16)\n1/week-commune (6 questions, CL 12th)\nStatistics\nStr 5, Dex 19, Con 12, Int 14, Wis 17, Cha 20\nBase Atk +3; CMB +5; CMD 12\nFeats Agile Maneuvers, Improved Initiative\nSkills Acrobatics +10, Bluff +11, Diplomacy +11, Fly +16, Knowledge (any one) +8, Perception +9, Perform (any one) +11, Spellcraft +5, Stealth +18\nLanguages Celestial, Draconic, Infernal; truespeech\nSQ traveler's friend\nEcology\nEnvironment any (Elysium)\nOrganization solitary, band (2-5), or company (6-24)\nTreasure none\nSpecial Abilities\nStarlight Blast (Su) As a standard action once every 1d4 rounds, a lyrakien can tap into the divine power of Elysium, unleashing a blast of holy starlight in a 5-foot burst. All creatures in this area take 1d4 points of holy damage, plus 1 point for each step their alignment deviates from chaotic good. For example, a chaotic neutral or neutral good creature would take 1d4+1 points of damage, a neutral creature would take 1d4+2 points of damage, and a lawful evil creature would take 1d4+4 points of damage. A DC 12 Reflex save negates this damage. Chaotic good creatures are unaffected by this ability. The save DC is Constitution-based.\n\nTraveler's Friend (Su) The performances and company of a lyrakien ease the burden of travel. Once per day, a creature may spend a minute listening to a lyrakien's performance-doing so removes the effects of exhaustion and fatigue from the listener.\nDescription\nLyrakien are divine musicians and messengers, mainly in the employ of deities of travel and natural wonders. They love to explore and visit beautiful places, especially locations with excellent views of rainbows, moonlight, and the stars. Whimsical and joyous, they love contests of song, dance, and knowledge, and keep journeys happy by distracting their companions from weary feet and stale food. Mortals who please them with excellent tales and new songs may be rewarded with elaborate maps, forgotten shortcuts, or rambling directions to hidden locations that hold lost magic.\n\nLyrakien are light-hearted creatures, but they are very protective of breathtaking natural locations. Often called 'glistenwings' by gnomes and halflings, lyrakien are frequently mistaken for fey-while they are generally friendly with true fey, their origin is the plane of Elysium. Like other azatas, they grow restless if they stay in one place too long. A chaotic good 7th-level spellcaster can gain a lyrakien as a familiar if she has the Improved Familiar feat.</pre>\n        </div>\n    "
  skills:
    acr:
      rank: 6
    blf:
      rank: 3
    dip:
      rank: 6
    fly:
      rank: 8
    ken:
      rank: 6
    kge:
      rank: 6
    kna:
      rank: 6
    per:
      rank: 3
    spl:
      rank: 3
    ste:
      rank: 3
  traits:
    ci:
      - petrification
    di:
      - electricity
    dr:
      custom: 5/evil
    eres:
      custom: Cold 10;Fire 10;Cold 10;Fire 10;Cold 10;Fire 10;
    languages:
      - celestial
      - null
    senses:
      custom: >-
        darkvision 60 ft., detect evil, detect magic, low-light vision;
        Perception +9
      dv:
        value: 60
    size: tiny
type: npc
