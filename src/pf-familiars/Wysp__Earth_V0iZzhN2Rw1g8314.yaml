_id: V0iZzhN2Rw1g8314
_key: '!actors!V0iZzhN2Rw1g8314'
_stats:
  coreVersion: '12.331'
img: icons/svg/mystery-man.svg
items:
  - _id: cV7yHt8i5YCV0ZTd
    _key: '!actors.items!V0iZzhN2Rw1g8314.cV7yHt8i5YCV0ZTd'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_40.jpg
    name: Outsider (Earth, Elemental)
    system:
      bab: high
      classSkills:
        blf: true
        crf: true
        kpl: true
        per: true
        sen: true
        ste: true
      description:
        value: >-
          <p>An outsider is at least partially composed of the essence (but not
          necessarily the material) of some plane other than the Material Plane.
          Some creatures start out as some other type and become outsiders when
          they attain a higher (or lower) state of spiritual
          existence.</p><h2>Features</h2><p>An outsider has the following
          features.</p><ul><li>d10 Hit Dice.</li><li>Base attack bonus equal to
          total Hit Dice (fast progression).</li><li>Two good saving throws,
          usually&nbsp;<a
          href="https://www.d20pfsrd.com/gamemastering/combat#TOC-Reflex"
          rel="nofollow">Reflex</a>&nbsp;and Will.</li><li>Skill points equal to
          6 + Int modifier (minimum 1) per Hit Die. The following are class
          skills for outsiders: <a href="https://www.d20pfsrd.com/skills/bluff"
          rel="nofollow">Bluff</a>, <a
          href="https://www.d20pfsrd.com/skills/craft" rel="nofollow">Craft</a>,
          <a href="https://www.d20pfsrd.com/skills/knowledge"
          rel="nofollow">Knowledge</a>&nbsp;(planes), <a
          href="https://www.d20pfsrd.com/skills/perception"
          rel="nofollow">Perception</a>, <a
          href="https://www.d20pfsrd.com/skills/sense-motive"
          rel="nofollow">Sense Motive</a>, and <a
          href="https://www.d20pfsrd.com/skills/stealth"
          rel="nofollow">Stealth</a>. Due to their varied nature, outsiders also
          receive 4 additional class skills determined by the creature’s
          theme.</li></ul><h2>Traits</h2><p>An outsider possesses the following
          traits (unless otherwise noted in a creature’s
          entry).</p><ul><li>Darkvision 60 feet.</li><li>Unlike most living
          creatures, an outsider does not have a dual nature—its soul and body
          form one unit. When an outsider is slain, no soul is set loose. Spells
          that restore souls to their bodies, such as&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/r/raise-dead"
          rel="nofollow">raise dead</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/r/reincarnate"
          rel="nofollow">reincarnate</a>,&nbsp;and&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/r/resurrection"
          rel="nofollow">resurrection</a>,&nbsp;don’t work on an outsider. It
          takes a different magical effect, such as&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/l/limited-wish"
          rel="nofollow">limited wish</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/w/wish"
          rel="nofollow">wish</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/m/miracle"
          rel="nofollow">miracle</a>,&nbsp;or&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/t/true-resurrection"
          rel="nofollow">true resurrection</a>&nbsp;to restore it to life. An
          outsider with the native subtype can be raised, reincarnated, or
          resurrected just as other living creatures can be.</li><li>Proficient
          with all simple and martial weapons and any weapons mentioned in its
          entry.</li><li>Proficient with whatever type of armor (light, medium,
          or heavy) it is described as wearing, as well as all lighter types.
          Outsiders not indicated as wearing armor are not proficient with
          armor. Outsiders are proficient with shields if they are proficient
          with any form of armor.</li><li>Outsiders breathe, but do not need to
          eat or sleep (although they can do so if they wish). Native outsiders
          breathe, eat, and sleep.</li></ul>
      hd: 10
      hp: 16
      level: 3
      savingThrows:
        ref:
          value: high
        will:
          value: high
      skillsPerLevel: 6
      tag: outsider
    type: class
  - _id: 3500b57gj8acnzDN
    _key: '!actors.items!V0iZzhN2Rw1g8314.3500b57gj8acnzDN'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_40.jpg
    name: 'Race: Outsider'
    system:
      creatureSubtypes:
        - earth
        - elemental
      creatureTypes:
        - outsider
      description:
        value: >-
          sbc | As the statblock did not include a race, a custom one was
          generated.
    type: race
  - _id: jLO9RYmTIO0A5enE
    _key: '!actors.items!V0iZzhN2Rw1g8314.jLO9RYmTIO0A5enE'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/shadow_12.jpg
    name: 'Sense: Darkvision 60ft.'
    system:
      abilityType: ex
      description:
        value: >-
          Darkvision is the extraordinary ability to see with no light source at
          all, out to a range specified for the creature. Darkvision is
          black-and-white only (colors cannot be discerned). It does not allow
          characters to see anything that they could not see otherwise—invisible
          objects are still invisible, and illusions are still visible as what
          they seem to be. Likewise, darkvision subjects a creature to gaze
          attacks normally. The presence of light does not spoil darkvision.
      subType: racial
    type: feat
  - _id: UH350QaAY6T76mUe
    _key: '!actors.items!V0iZzhN2Rw1g8314.UH350QaAY6T76mUe'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/violet_14.jpg
    name: 'Sense: Tremorsense'
    system:
      abilityType: ex
      description:
        value: >-
          A creature with tremorsense is sensitive to vibrations in the ground
          and can automatically pinpoint the location of anything that is in
          contact with the ground.</p><p>Aquatic creatures with tremorsense can
          also sense the location of creatures moving through water. The
          ability’s range is specified in the creature’s descriptive text.
      subType: racial
    type: feat
  - _id: gnDQwMjvOPPEiQNV
    _key: '!actors.items!V0iZzhN2Rw1g8314.gnDQwMjvOPPEiQNV'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/spells/runes-blue-3.jpg
    name: 'Aura: Resonance'
    system:
      actions:
        - _id: uyfzzqg0tfeevqax
          activation:
            cost: null
            type: passive
            unchained:
              cost: null
              type: passive
          duration:
            units: perm
          img: systems/pf1/icons/spells/runes-blue-3.jpg
          measureTemplate:
            size: '30'
            type: circle
          name: Use
          range:
            units: ft
            value: '30'
          save:
            type: will
          tag: auraResonance
          target:
            value: centered on self
      subType: racial
    type: feat
  - _id: 8snLqsJN4LLL00Nq
    _key: '!actors.items!V0iZzhN2Rw1g8314.8snLqsJN4LLL00Nq'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/feats/toughness.jpg
    name: Toughness
    system:
      changes:
        - _id: bl5pemy2
          formula: max(3, @attributes.hd.total)
          target: mhp
          type: untyped
      description:
        value: |2-

                          <em>You have enhanced physical stamina.</em></p><p>
                          <p><strong>Benefits</strong></p><p>You gain +3 hit points. For every Hit Die you possess beyond 3, you gain an additional +1 hit point. If you have more than 3 Hit Dice, you gain +1 hit points whenever you gain a Hit Die (such as when you gain a level).</p>
      tags:
        - General
        - Defensive
    type: feat
  - _id: eAQ2kJjHABVxGJ5f
    _key: '!actors.items!V0iZzhN2Rw1g8314.eAQ2kJjHABVxGJ5f'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Special Quality: Living Battery'
    system:
      description:
        value: >-
          sbc | Placeholder for Special Qualities, which in most statblocks are
          listed under SQ in the statistics block, but described in the Special
          Abilities. Remove duplicates as needed!
      subType: misc
    type: feat
  - _id: mtA7OngiDY9cThsx
    _key: '!actors.items!V0iZzhN2Rw1g8314.mtA7OngiDY9cThsx'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Special Quality: Servitor'
    system:
      description:
        value: >-
          sbc | Placeholder for Special Qualities, which in most statblocks are
          listed under SQ in the statistics block, but described in the Special
          Abilities. Remove duplicates as needed!
      subType: misc
    type: feat
  - _id: 15ALUqVPnbV2lBdm
    _key: '!actors.items!V0iZzhN2Rw1g8314.15ALUqVPnbV2lBdm'
    _stats:
      coreVersion: '12.331'
    img: icons/svg/mystery-man.svg
    name: Tendrils
    system:
      actions:
        - _id: 2y9qubl3qdbsukpr
          ability:
            attack: _default
            damage: str
          actionType: mwak
          activation:
            type: attack
            unchained:
              type: attack
          attackBonus: '2'
          attackName: Tendrils
          damage:
            parts:
              - formula: 1d3+2
          duration:
            units: inst
          img: icons/svg/mystery-man.svg
          name: Attack
          notes:
            footer:
              - 2 tendrils +9 (1d3+4)
          range:
            units: melee
          tag: tendrils
      subType: weapon
    type: attack
  - _id: gt3fsdE3ME3Lbf4U
    _key: '!actors.items!V0iZzhN2Rw1g8314.gt3fsdE3ME3Lbf4U'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: sbc | Conversion Buff
    system:
      active: true
      changes:
        - _id: 0px59ly6
          formula: '-1'
          target: cmb
          type: untypedPerm
        - _id: xdpw0a7e
          formula: '2'
          target: fort
          type: untypedPerm
        - _id: 6k2v1f5w
          formula: '-2'
          target: will
          type: untypedPerm
      description:
        value: >-
          <h2>sbc | Conversion Buff</h2><p>This Buff was created by
          <strong>sbc</strong> to compensate for differences between the input
          and the values FoundryVTT calculates automatically.</p><p>Especially
          when the pathfinder system gets upgraded, entries in compendiums get
          updated or foundry changes in some way, this buff may become
          outdated.</p><p>For most mooks the conversion should more or less
          work, but for important NPCs or creatures it is adviced to double
          check the conversion manually.</p>
      hideFromToken: true
      subType: perm
      tag: sbcConversionBuff
    type: buff
name: Wysp, Earth
prototypeToken:
  texture:
    src: icons/svg/mystery-man.svg
system:
  abilities:
    cha:
      value: 13
    con:
      value: 14
    dex:
      value: 13
    str:
      value: 14
    wis:
      value: 13
  attributes:
    cmdNotes: can't be tripped
    naturalAC: '+1'
    speed:
      burrow:
        base: 20
      land:
        base: 20
    spells:
      spellbooks:
        primary:
          name: Primary
        secondary:
          name: Secondary
        spelllike:
          name: Spell-likes
        tertiary:
          name: Tertiary
  details:
    cr:
      base: 2
    notes:
      value: "\n        <div class=\"statblockContainer\" style=\"margin-top: 15px\"><div style=\"width: 100%; background-color: #ffe9c7; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); columns: 2 150px\">\r\n    <hr style=\"margin-left: 0; margin-top:-2px; width: 100%; height: 4px; background-color: #e0a100; border: 1px solid #000; column-span: all;\" />\r\n    <div style=\"padding: 5px;\">\r\n        <h1>Earth Wysp CR 2 </h1>\r\n        \r\n        <!-- BASE -->\r\n        <div style=\"break-inside: avoid;\">\r\n            <strong>Source</strong>: Bestiary 5 pg. 282<br/>\r\n            <strong>XP 600</strong><br/>\r\n            \r\n            \r\n            \r\n            \r\n            \r\n            N \r\n            Tiny \r\n            Outsider (Earth, Elemental) <br/>\r\n            <strong>Init</strong> 1;\r\n            <strong>Senses</strong> darkvision 60 ft., tremorsense 30 ft.; Perception +7; \r\n            <strong>Aura</strong>  resonance (30 ft.)\r\n        </div>\r\n        <div style=\"break-inside: avoid;\">\r\n            <h3>Defense</h3>\r\n                <strong>AC</strong> 14, <strong>Touch</strong> 13, <strong>Flat-Footed</strong> 13 (+1 Dex, +1 natural, +2 size)<br/>\r\n            <strong>Hit Points</strong> 25 (3 HD; 3d10+9)<br/>\r\n            <strong>Fort</strong> 5, <strong>Ref</strong> 4, <strong>Will</strong> 2\r\n            \r\n            <strong>DR</strong> 1/-\r\n            <strong>Immune</strong> elemental traits; \r\n            \r\n            \r\n            \r\n            \r\n        </div>\r\n        <div>\r\n            <div style=\"break-inside: avoid;\">\r\n                <h3>Offense</h3>\r\n                        <strong>Speed</strong> 20 ft.<br/>\r\n                <strong>Melee</strong> 2 tendrils +9 (1d3+4)<br/>\r\n                \r\n                \r\n                \r\n                \r\n            </div>\r\n        </div>\r\n        <!-- SPELLCASTING -->\r\n                \r\n        <!-- TACTICS -->\r\n                \r\n                \r\n                \r\n                \r\n        <!-- STATISTICS -->\r\n        <div style=\"break-inside: avoid;\">\r\n            <h3>Statistics</h3>\r\n                <strong>Str</strong> 14, <strong>Dex</strong> 13, <strong>Con</strong> 14, <strong>Int</strong> 10, <strong>Wis</strong> 13, <strong>Cha</strong> 13<br/>\r\n            <strong>Base Atk</strong> +3; <strong>CMB</strong> +2; <strong>CMD</strong> 14<br/>\r\n            <strong>Feats</strong> Power Attack, Toughness<br/>\r\n            <strong>Skills</strong> Bluff +7, Climb +8, Knowledge (dungeoneering, engineering, planes) +5, Perception +7, Sense Motive +7<br/>\r\n            <strong>Languages</strong> Terran<br/>\r\n            \r\n            \r\n            \r\n            \r\n        </div>\r\n        <!-- ECOLOGY -->\r\n                \r\n                \r\n                \r\n        <!-- SPECIAL ABILITIES -->\r\n        \r\n        <!-- DESCRIPTION -->\r\n        \r\n                    \r\n    </div>\r\n    <hr style=\"margin-left: 0; margin-bottom: -3px; width: 100%; height: 4px; background-color: #e0a100; border: 1px solid #000; column-span: all;\" />\r\n</div></div>\n    \n        <br>\n        <hr>\n        <div class=\"rawInputContainer\" style=\"margin-top: 15px;\">\n            <h2>RAW INPUT</h2>\n            <hr>\n            <pre style=\"white-space: pre-wrap; font-size: 10px;\">Earth Wysp CR 2\nSource Bestiary 5 pg. 282\nXP 600\nN Tiny outsider (earth, elemental)\nInit +1; Senses darkvision 60 ft., tremorsense 30 ft.; Perception +7\nAura resonance (30 ft.)\nDefense\nAC 14, touch 13, flat-footed 13 (+1 Dex, +1 natural, +2 size)\nhp 25 (3d10+9)\nFort +5, Ref +4, Will +2\nDR 1/-; Immune elemental traits\nOffense\nSpeed 20 ft., burrow 20 ft.\nMelee 2 tendrils +9 (1d3+4)\nSpace 2-1/2 ft., Reach 0 ft.\nStatistics\nStr 14, Dex 13, Con 14, Int 10, Wis 13, Cha 13\nBase Atk +3; CMB +2; CMD 14 (can't be tripped)\nFeats Power Attack, Toughness\nSkills Bluff +7, Climb +8, Knowledge (dungeoneering, engineering, planes) +5, Perception +7, Sense Motive +7\nLanguages Terran\nSQ living battery, servitor\n\nThere is no description for this monster.</pre>\n        </div>\n    "
  skills:
    blf:
      rank: 3
    clm:
      rank: 6
    kdu:
      rank: 5
    ken:
      rank: 5
    kpl:
      rank: 2
    per:
      rank: 3
    sen:
      rank: 3
  traits:
    dr:
      custom: 1/-
    languages:
      - terran
    senses:
      custom: darkvision 60 ft., tremorsense 30 ft.; Perception +7
      dv:
        value: 60
    size: tiny
type: npc
